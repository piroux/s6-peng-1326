/*
 *  example.c - an example graph program
 *  Copyright (C) 2010 Martin Broadhurst
 *  www.martinbroadhurst.com
 */

#include <assert.h>
#include <stdio.h>

#include <graph2.h>

int main(void)
{
    MBgraph2 *graph;
    MBvertex *vertex;
    MBvertex *A, *B, *C, *D, *E;
    MBiterator *vertices, *edges;
    MBedge *edge;

    /* Create a graph */
    graph = MBgraph2_create();

    /* Add vertices */
    A = MBgraph2_add(graph, "A", NULL);
    B = MBgraph2_add(graph, "B", NULL);
    C = MBgraph2_add(graph, "C", NULL);
    D = MBgraph2_add(graph, "D", NULL);
    E = MBgraph2_add(graph, "E", NULL);

    /* Add edges */
    MBgraph2_add_edge(graph, A, B);
    MBgraph2_add_edge(graph, A, D);
    MBgraph2_add_edge(graph, B, C);
    MBgraph2_add_edge(graph, C, B);
    MBgraph2_add_edge(graph, D, A);
    MBgraph2_add_edge(graph, D, C);
    MBgraph2_add_edge(graph, D, E);

    /* Display */
    printf("Vertices (%d) and their neighbours:\n\n", MBgraph2_get_vertex_count(graph));
    vertices = MBgraph2_get_vertices(graph);
    while ((vertex = MBiterator_get(vertices))) {
        MBiterator *neighbours;
        MBvertex *neighbour;
        unsigned int n = 0;
        printf("%s (%d): ", MBvertex_get_name(vertex), MBgraph2_get_neighbour_count(graph, vertex));
        neighbours = MBgraph2_get_neighbours(graph, vertex);
        while ((neighbour = MBiterator_get(neighbours))) {
            printf("%s", MBvertex_get_name(neighbour));
            if (n < MBgraph2_get_neighbour_count(graph, vertex) - 1) {
                fputs(", ", stdout);
            }
            n++;
        }
        putchar('\n');
        MBiterator_delete(neighbours);
    }
    putchar('\n');
    MBiterator_delete(vertices);
    printf("Edges (%d):\n\n", MBgraph2_get_edge_count(graph));
    edges = MBgraph2_get_edges(graph);
    while ((edge = MBiterator_get(edges))) {
        printf("<%s, %s>\n", MBvertex_get_name(MBedge_get_from(edge)), MBvertex_get_name(MBedge_get_to(edge)));
    }
    putchar('\n');



    edges = MBgraph2_get_edges(graph);
    while ((edge = MBiterator_get(edges))) {
      //MBvertex_get_name(MBedge_get_from(edge));
      const char *name1 = MBvertex_get_name(MBedge_get_from(edge));
      printf("found name src: %s (%p)\n", name1, name1);
      MBvertex *v1 = MBgraph2_get_vertex(graph, name1);
      assert(v1 != NULL);
      const char *name2 = MBvertex_get_name(MBedge_get_to(edge));
      printf("found name dst: %s (%p)\n", name2, name2);
      MBvertex *v2 = MBgraph2_get_vertex(graph, name2);
      assert(v2 != NULL);
      //MBgraph2_add_edge(gd_dst->graph, v1, v2);
    }


    MBiterator_delete(edges);

    /* Delete */
    MBgraph2_delete(graph);

    return 0;
}
