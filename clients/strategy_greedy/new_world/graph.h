
#ifndef ____graph__
#define ____graph__

#include <stdio.h>
#include <stdlib.h>
#include "interface_strategy_server.h"

#define MAX_TILE_LINE 30
#define MAX_ADJ 6

struct graph{
    struct tile* tiles;
    int nb_tiles;
};

struct tile{
    unsigned int id;
    unsigned int nb_fish;
    
    int id_peng;
    int id_player;
    int is_in;
    
    //table of adjacent accessible tile
    int adj[MAX_ADJ];
};



void tile_init(struct tile* t, unsigned int id, unsigned int nb_fish);
void graph__construct(struct graph* g);
int nb_tiles_total();


#endif /* defined(____graph__) */
