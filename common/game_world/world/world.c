
#include "world.h"


/* Initialisateur du sous-modèle de jeu */
void world__init (struct world *w, char **player_names, int nb_players, int nb_penguins_per_player,  int nb_tiles, enum tessel_kind tk) {
  assert(nb_players <= MAX_PLAYERS);
  assert(nb_penguins_per_player <= MAX_PENGUINS_PER_PLAYER);
  assert(nb_tiles <= MAX_TILES);

  /* World defaults */
  w->nb_players = nb_players;
  w->nb_penguins_per_player = nb_penguins_per_player;

  for(int i = 0; i < nb_players; i++) {
    player__init(&w->players[i], w->nb_penguins_per_player, player_names[i]);
  }

  grid__init(&w->grid_board, nb_tiles, tk);
}


/* Méthode de copie de sous-modèles de jeu */
void world__copy (struct world *w_dst, struct world *w_src) {
  w_dst->nb_players = w_src->nb_players;
  w_dst->nb_penguins_per_player = w_src->nb_penguins_per_player;

  for(int i = 0; i < w_src->nb_players; i++) {
    player__copy(&w_dst->players[i], &w_src->players[i]);
  }

  grid__copy(&w_dst->grid_board, &w_src->grid_board);
}


/* Déstructeur de sous-modèle de jeu */
void world__destroy (struct world *w) {
  for(int i = 0; i < w->nb_players; i++) {
    player__destroy(&w->players[i]);
  }

  grid__destroy(&w->grid_board);
}
