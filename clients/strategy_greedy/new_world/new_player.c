
#include "new_player.h"

//Construct a structure of player from interface functions
void player__construct (struct graph* g, struct player *pl, int id_player, int nb_penguins_per_player) {

    pl->id = id_player;
    pl->nb_penguins_max = nb_penguins_per_player;

    for (int i = 0; i < pl->nb_penguins_max; i++) 
        penguin__construct(g,&pl->penguins[i], pl->id, i);
}




