/*
 *  graph3.c - a graph as an adjacency matrix
 *  Copyright (C) 2010 Martin Broadhurst 
 *  www.martinbroadhurst.com
 */

#include <stdlib.h>

#include <edge.h>

#include <graph3.h>

static const unsigned int connected = 1;

MBgraph3 *MBgraph3_create(void)
{
	MBgraph3 *graph = malloc(sizeof(MBgraph3));
	if (graph) {
		graph->vertices = MBset_create((MBcmpfn)MBvertex_compare);
		graph->edges = MBmatrix_create(0, 0);
	}

	return graph;
}

void MBgraph3_delete(MBgraph3 *graph)
{
	if (graph) {
		MBset_for_each(graph->vertices, (MBforfn)MBvertex_delete);
		MBset_delete(graph->vertices);
		MBmatrix_delete(graph->edges);
		free(graph);
	}
}

MBvertex *MBgraph3_add(MBgraph3 *graph, const char *name, void *data)
{
	MBvertex *vertex = MBvertex_create(name, data, NULL, NULL);
	MBvertex *existing = MBset_add(graph->vertices, vertex);
	const unsigned int v = MBset_find_index(graph->vertices, vertex);
	MBvertex_delete(existing);
	MBmatrix_insert_rowcolumn(graph->edges, v);
	return vertex;
}

MBvertex *MBgraph3_get_vertex(const MBgraph3 *graph, const char *name)
{
	return MBset_find(graph->vertices, &name);
}

void *MBgraph3_remove(MBgraph3 *graph, MBvertex *vertex)
{
	const unsigned int v = MBset_find_index(graph->vertices, vertex);
	MBvertex *removed;
	void *data;
	MBmatrix_remove_rowcolumn(graph->edges, v);
	removed = MBset_remove(graph->vertices, vertex);
	data = vertex->data;
	MBvertex_delete(removed);
	return data;
}

void MBgraph3_add_edge(MBgraph3 *graph, MBvertex *vertex1, MBvertex *vertex2)
{
	const int v1 = MBset_find_index(graph->vertices, vertex1);
	const int v2 = MBset_find_index(graph->vertices, vertex2);

	if (v1 != -1 && v2 != -1) {
		MBmatrix_set(graph->edges, v1, v2, connected);
	}
}

void MBgraph3_remove_edge(MBgraph3 *graph, MBvertex *vertex1, MBvertex *vertex2)
{
	const int v1 = MBset_find_index(graph->vertices, vertex1);
	const int v2 = MBset_find_index(graph->vertices, vertex2);

	if (v1 != -1 && v2 != -1) {
		MBmatrix_set(graph->edges, v1, v2, 0);
	}
}

unsigned int MBgraph3_get_adjacent(const MBgraph3 *graph, const MBvertex *vertex1, const MBvertex *vertex2)
{
	const int v1 = MBset_find_index(graph->vertices, vertex1);
	const int v2 = MBset_find_index(graph->vertices, vertex2);
	unsigned int adjacent = 0;
	if (v1 != -1 && v2 != -1) {
		adjacent = MBmatrix_get(graph->edges, v1, v2) == connected;
	}
	return adjacent;
}

typedef struct {
	const MBgraph3 *graph;
	unsigned int row;
	unsigned int column;
} neighbour_iterator;

static neighbour_iterator *neighbour_iterator_create(const MBgraph3 *graph, const unsigned int row)
{
	neighbour_iterator *it = malloc(sizeof(neighbour_iterator));
	if (it) {
		it->graph = graph;
		it->row = row;
		it->column = 0;
	}
	return it;
}

static void neighbour_iterator_delete(neighbour_iterator *it)
{
	free(it);
}

static void *neighbour_iterator_get(neighbour_iterator *it)
{
	MBvertex *neighbour = NULL;
	for (;it->column < MBset_get_count(it->graph->vertices) && neighbour == NULL; it->column++) {
		if (MBmatrix_get(it->graph->edges, it->row, it->column) == connected) {
			neighbour = MBset_get(it->graph->vertices, it->column);
		}
	}
	return neighbour;
}

MBiterator *MBgraph3_get_neighbours(const MBgraph3 *graph, const MBvertex *vertex)
{
	const unsigned int row = MBset_find_index(graph->vertices, vertex);
	return MBiterator_create(neighbour_iterator_create(graph, row), (MBgetfn)neighbour_iterator_get,
		(MBdeletefn)neighbour_iterator_delete);
}

typedef struct {
	const MBgraph3 *graph;
	unsigned int fromindex;		/* Index of the current outgoing vertex */
	unsigned int toindex;		/* Index of the current incoming vertex */
	MBedge *edge;
} edge_iterator;

static edge_iterator *edge_iterator_create(const MBgraph3 *graph)
{
	edge_iterator *it = malloc(sizeof(edge_iterator));
	if (it) {
		it->graph = graph;
		it->edge = malloc(sizeof(MBedge));
		it->fromindex = 0;
		if (MBset_get_count(graph->vertices)) {
			MBvertex *vertex = MBset_get(it->graph->vertices, 0);
			it->edge->from = vertex;
			it->toindex = 0;
		}
	}
	return it;
}

static void edge_iterator_delete(edge_iterator *it)
{
	if (it) {
		free(it->edge);
		free(it);
	}
}

static void *edge_iterator_get(edge_iterator *it)
{
	MBedge *edge = NULL;
	const unsigned int size = MBset_get_count(it->graph->vertices);
	
	while (it->fromindex < size && edge == NULL) {
		for (; it->toindex < size && edge == NULL; it->toindex++) {
			if (MBmatrix_get(it->graph->edges, it->fromindex, it->toindex) == connected) {
				it->edge->from = MBset_get(it->graph->vertices, it->fromindex);
				it->edge->to = MBset_get(it->graph->vertices, it->toindex);
				edge = it->edge;
			}
		}
		if (edge == NULL) {
			/* Move to the next row */
			it->fromindex++;
			it->toindex = 0;
		}
	}
	return edge;
}

MBiterator *MBgraph3_get_edges(const MBgraph3 *graph)
{
	return MBiterator_create(edge_iterator_create(graph), (MBgetfn)edge_iterator_get, 
		(MBdeletefn)edge_iterator_delete);
}

MBiterator *MBgraph3_get_vertices(const MBgraph3 *graph)
{
	return MBset_iterator(graph->vertices);
}

unsigned int MBgraph3_get_neighbour_count(const MBgraph3 * graph, const MBvertex * vertex)
{
	/* The neighbour count is the number of 1s in this vertex's row in the adjacency matrix */
	unsigned int r = MBset_find_index(graph->vertices, vertex);
	unsigned int c;
	unsigned int neighbours = 0;
	for (c = 0; c < MBset_get_count(graph->vertices); c++) {
		neighbours += MBmatrix_get(graph->edges, r, c);
	}
	return neighbours;
}

unsigned int MBgraph3_get_edge_count(const MBgraph3 * graph)
{
	/* The edge count is the total number of 1s in the adjacency matrix */
	unsigned int r, c;
	unsigned int edges = 0;

	for (r = 0; r < MBset_get_count(graph->vertices); r++) {
		for (c = 0; c < MBset_get_count(graph->vertices); c++) {
			edges += MBmatrix_get(graph->edges, r, c);
		}
	}
	return edges;
}

unsigned int MBgraph3_get_vertex_count(const MBgraph3 * graph)
{
	return MBset_get_count(graph->vertices);
}
