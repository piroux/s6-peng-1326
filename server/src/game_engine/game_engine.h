
#ifndef __GAME_ENGINE_H__
#define __GAME_ENGINE_H__

#include <stdlib.h>

#include "log.h"

#include "../world_engine/world_engine.h"
#include "../strategy_engine/strategy_engine.h"
#include "game_parameters.h"


//++ #include "../display/display_game.h"

struct game_engine {
  struct world_engine wd_eng;
  struct strategy_engine st_eng;

  struct game_parameters gp;

  //enum game_state gs;
};


void game_engine__setup(struct game_engine *gm_eng);
void game_engine__cleanup(struct game_engine *gm_eng);
void game_engine__next(struct game_engine *gm_eng);

void game_engine__report(struct game_engine *gm_eng);

int game_exec(struct game_parameters gp);

#endif
