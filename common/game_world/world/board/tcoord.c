
#include "tcoord.h"


void tcoord__set(struct tcoord *c, int x, int y) {
  c->x = x;
  c->y = y;
  sprintf(c->s, "%d:%d", c->x, c->y);
}

void tcoord__copy(struct tcoord *c_dst, struct tcoord *c_src) {
  tcoord__set(c_dst, c_src->x, c_src->y);
}


void tcoord__diff(struct tcoord *c_diff, struct tcoord *c2, struct tcoord *c1) {
  tcoord__set(c_diff, c2->x - c1->x, c2->y - c1->y);
}
