/*
 *  graph4.c - a graph as an incidence matrix
 *  Copyright (C) 2010 Martin Broadhurst 
 *  www.martinbroadhurst.com
 */

#include <stdlib.h>

#include <edge.h>

#include <graph4.h>

const unsigned int from = 1;
const unsigned int to   = 2;

MBgraph4 *MBgraph4_create(void)
{
	MBgraph4 *graph = malloc(sizeof(MBgraph4));
	if (graph) {
		graph->vertices = MBset_create((MBcmpfn)MBvertex_compare);
		graph->edges = MBmatrix_create(0, 0);
	}

	return graph;
}

void MBgraph4_delete(MBgraph4 *graph)
{
	if (graph) {
		MBset_for_each(graph->vertices, (MBforfn)MBvertex_delete);
		MBset_delete(graph->vertices);
		MBmatrix_delete(graph->edges);
		free(graph);
	}
}

MBvertex *MBgraph4_add(MBgraph4 *graph, const char *name, void *data)
{
	MBvertex *vertex = MBvertex_create(name, data, NULL, NULL);
	MBvertex *existing = MBset_add(graph->vertices, vertex);
	const unsigned int v = MBset_find_index(graph->vertices, vertex);
	MBvertex_delete(existing);
	MBmatrix_insert_row(graph->edges, v);
	return vertex;
}

MBvertex *MBgraph4_get_vertex(const MBgraph4 *graph, const char *name)
{
	return MBset_find(graph->vertices, &name);
}

void *MBgraph4_remove(MBgraph4 *graph, MBvertex *vertex)
{
	const unsigned int v = MBset_find_index(graph->vertices, vertex);
	MBvertex *removed;
	void *data;
	unsigned int c = 0;
	while (c < MBmatrix_get_column_count(graph->edges)) {
		/* If column c is an edge incident on vertex, remove it */
		const unsigned int entry = MBmatrix_get(graph->edges, v, c);
		if (entry == from || entry == to) {
			MBmatrix_remove_column(graph->edges, c);
		}
		else {
			c++;
		}
	}
	removed = MBset_remove_at(graph->vertices, v);
	data = vertex->data;
	MBvertex_delete(vertex);
	return data;
}

void MBgraph4_add_edge(MBgraph4 *graph, MBvertex *vertex1, MBvertex *vertex2)
{
	const int v1 = MBset_find_index(graph->vertices, vertex1);
	const int v2 = MBset_find_index(graph->vertices, vertex2);
	unsigned int column;

	/* Add a column from v1 to v2 */
	MBmatrix_add_column(graph->edges);
	column = MBmatrix_get_column_count(graph->edges) - 1;
	MBmatrix_set(graph->edges, v1, column, from);
	MBmatrix_set(graph->edges, v2, column, to);
}

void MBgraph4_remove_edge(MBgraph4 *graph, MBvertex *vertex1, MBvertex *vertex2)
{
	const int v1 = MBset_find_index(graph->vertices, vertex1);
	const int v2 = MBset_find_index(graph->vertices, vertex2);
	unsigned int c;
	unsigned int removed = 0;

	/* Find a column from v1 to v2 */
	for (c = 0; c < MBmatrix_get_column_count(graph->edges) && !removed; c++) {
		if (MBmatrix_get(graph->edges, v1, c) == from && MBmatrix_get(graph->edges, v2, c) == to) {
			MBmatrix_remove_column(graph->edges, c);
			removed = 1;
		}
	}
}

unsigned int MBgraph4_get_adjacent(const MBgraph4 *graph, const MBvertex *vertex1, const MBvertex *vertex2)
{
	const int v1 = MBset_find_index(graph->vertices, vertex1);
	const int v2 = MBset_find_index(graph->vertices, vertex2);
	unsigned int adjacent = 0;
	unsigned int c;

	for (c = 0; c < MBmatrix_get_column_count(graph->edges) && !adjacent; c++) {
		adjacent = MBmatrix_get(graph->edges, v1, c) == from && MBmatrix_get(graph->edges, v2, c) == to;
	}

	return adjacent;
}

typedef struct {
	const MBgraph4 *graph;
	unsigned int row;
	unsigned int column;
} neighbour_iterator;

static neighbour_iterator *neighbour_iterator_create(const MBgraph4 *graph, const unsigned int row)
{
	neighbour_iterator *it = malloc(sizeof(neighbour_iterator));
	if (it) {
		it->graph = graph;
		it->row = row;
		it->column = 0;
	}
	return it;
}

static void neighbour_iterator_delete(neighbour_iterator *it)
{
	free(it);
}

static void *neighbour_iterator_get(neighbour_iterator *it)
{
	MBvertex *neighbour = NULL;
	for (;it->column < MBmatrix_get_column_count(it->graph->edges) && neighbour == NULL; it->column++) {
		if (MBmatrix_get(it->graph->edges, it->row, it->column) == from) {
			unsigned int r;
			for (r = 0; r < MBmatrix_get_row_count(it->graph->edges) && neighbour == NULL; r++) {
				if (MBmatrix_get(it->graph->edges, r, it->column) == to) {
					neighbour = MBset_get(it->graph->vertices, r);
				}
			}
		}
	}
	return neighbour;
}

MBiterator *MBgraph4_get_neighbours(const MBgraph4 *graph, const MBvertex *vertex)
{
	const unsigned int row = MBset_find_index(graph->vertices, vertex);
	return MBiterator_create(neighbour_iterator_create(graph, row), (MBgetfn)neighbour_iterator_get, 
		(MBdeletefn)neighbour_iterator_delete);
}

typedef struct {
	const MBgraph4 *graph;
	unsigned int fromindex;		/* Index of the current outgoing vertex */
	unsigned int edgeindex;		/* Index of the current edge */
	MBedge *edge;
} edge_iterator;

static edge_iterator *edge_iterator_create(const MBgraph4 *graph)
{
	edge_iterator *it = malloc(sizeof(edge_iterator));
	if (it) {
		it->graph = graph;
		it->edge = malloc(sizeof(MBedge));
		it->fromindex = 0;
		if (MBset_get_count(graph->vertices)) {
			MBvertex *vertex = MBset_get(it->graph->vertices, 0);
			it->edge->from = vertex;
			it->edgeindex = 0;
		}
	}
	return it;
}

static void edge_iterator_delete(edge_iterator *it)
{
	if (it) {
		free(it->edge);
		free(it);
	}
}

static void *edge_iterator_get(edge_iterator *it)
{
	MBedge *edge = NULL;
	
	while (it->fromindex < MBset_get_count(it->graph->vertices) && edge == NULL) {
		for (; it->edgeindex < MBmatrix_get_column_count(it->graph->edges) && edge == NULL; it->edgeindex++) {
			if (MBmatrix_get(it->graph->edges, it->fromindex, it->edgeindex) == from) {
				unsigned int row;
				for (row = 0; row < MBmatrix_get_row_count(it->graph->edges) && edge == NULL; row++) {
					if (MBmatrix_get(it->graph->edges, row, it->edgeindex) == to) {
						it->edge->from = MBset_get(it->graph->vertices, it->fromindex);
						it->edge->to = MBset_get(it->graph->vertices, row);
						edge = it->edge;
					}
				}
			}
		}
		if (edge == NULL) {
			/* Move to the next row */
			it->fromindex++;
			it->edgeindex = 0;
		}
	}
	return edge;
}

MBiterator *MBgraph4_get_edges(const MBgraph4 *graph)
{
	return MBiterator_create(edge_iterator_create(graph), (MBgetfn)edge_iterator_get,
		(MBdeletefn)edge_iterator_delete);
}

MBiterator *MBgraph4_get_vertices(const MBgraph4 *graph)
{
	return MBset_iterator(graph->vertices);
}

unsigned int MBgraph4_get_neighbour_count(const MBgraph4 * graph, const MBvertex * vertex)
{
	/* The neighbour count is the number of columns in the incidence
	   matrix that have a from entry in this vertex's row */
	const unsigned int r = MBset_find_index(graph->vertices, vertex);
	unsigned int c;
	unsigned int neighbours = 0;

	for (c = 0; c < MBmatrix_get_column_count(graph->edges); c++) {
		neighbours += MBmatrix_get(graph->edges, r, c) == from;
	}

	return neighbours;
}

unsigned int MBgraph4_get_edge_count(const MBgraph4 * graph)
{
	/* The edge count is the number of columns in the incidence matrix */
	return MBmatrix_get_column_count(graph->edges);
}

unsigned int MBgraph4_get_vertex_count(const MBgraph4 * graph)
{
	return MBset_get_count(graph->vertices);
}
