
#include <stdlib.h>
#include <stdio.h>

#include "game_world.h"


void test__game_world__init(struct game_world* gw, int nb_players, int nb_penguins_per_player, int nb_tile, char*player_names[]){

  assert( gw->current_player_id == 0);
  assert( gw->current_round == 0);

  //world__init
  assert(gw->w.nb_players == nb_players);
  assert(gw->w.nb_penguins_per_player == nb_penguins_per_player);

  //player__init
  assert(gw->w.players[0].id == 0);
  assert(gw->w.players[0].nb_penguins_max == nb_penguins_per_player);
  assert(gw->w.players[0].nb_penguins_placed == 0);
  assert(gw->w.players[0].score_tiles == 0);
  assert(gw->w.players[0].score_fishes == 0);
  assert(gw->w.players[0].score_distance == 0);

  assert(gw->w.players[1].id == 1);
  assert(gw->w.players[1].nb_penguins_max == nb_penguins_per_player);
  assert(gw->w.players[1].nb_penguins_placed == 0);
  assert(gw->w.players[1].score_tiles == 0);
  assert(gw->w.players[1].score_fishes == 0);
  assert(gw->w.players[1].score_distance == 0);

  //penguin__init
  assert(gw->w.players[1].penguins[1].id == nb_penguins_per_player + 1);
  assert(gw->w.players[1].penguins[1].player_id == 1);
  assert(gw->w.players[1].penguins[1].penguin_player_id == 1);
  assert(gw->w.players[1].penguins[1].tile_id == -1);

  //grid__init
  assert(gw->w.grid_board.nb_tiles == nb_tile);

  //tile__init
  assert(gw->w.grid_board.tiles[0].id == 0);
  assert(gw->w.grid_board.tiles[20].id == 20);
  assert(gw->w.grid_board.tiles[20].nb_fish < 4);
  assert(gw->w.grid_board.tiles[20].nb_fish > 0);
  assert(gw->w.grid_board.tiles[20].penguin_id == -1);
  assert(gw->w.grid_board.tiles[20].coord.x == gw->w.grid_board.tiles[20].id % gw->w.grid_board.tessel_grid.tiles_per_axis);
  assert(gw->w.grid_board.tiles[20].coord.y == gw->w.grid_board.tiles[20].id / gw->w.grid_board.tessel_grid.tiles_per_axis);

  //tessel__init
  assert(gw->w.grid_board.tessel_grid.kind == TESSEL_HEXA);
  assert(gw->w.grid_board.tessel_grid.nb_directions == 6);
  assert(gw->w.grid_board.tessel_grid.tiles_per_axis == ((unsigned int) sqrt ((float) nb_tile)) + 1);

  printf("test__game_world__init  \t\t\tréussie\n");
}


void test_place__game_world__apply_action(struct game_world* gw, struct game_action* ga, int nb_players, int nb_penguins_per_player, int nb_tile){

  game_action__do_place(ga, 0, 2);
  game_world__apply_action(gw, ga);
  /*place a penguin on a tile :
   - The first penguin of the first player is set correctly
   - The other penguins are not placed
   */
  assert(ga->count == 1);
  assert(ga->kind == ACTION_PLACE);
  assert(gw->w.players[0].nb_penguins_placed == 1);
  assert(gw->w.players[1].nb_penguins_placed == 0);
  assert(gw->w.players[0].penguins[0].tile_id == 2);
  assert(gw->w.grid_board.tiles[2].penguin_id == gw->w.players[0].penguins[0].penguin_player_id);
  /*
  assert(gw->w.players[0].penguins[0].score_fishes == gw->w.grid_board.tiles[2].nb_fish);
  assert(gw->w.players[0].score_fishes == gw->w.grid_board.tiles[2].nb_fish);
  assert(gw->w.players[0].penguins[0].score_tiles == 1);
  assert(gw->w.players[0].score_tiles == 1);
  */
  assert(gw->current_player_id == 0);
  assert(gw->current_round == 0);

  game_world__prepare_next_round(gw);
  game_action__do_place(ga, 1, 3);
  game_world__apply_action(gw, ga);
  /*place the second penguin on a tile :
  - This is the second player turn
  - The first penguin of the second player is set correctly
  - The other penguins of the second player are not placed
  */
  assert(gw->w.players[0].nb_penguins_placed == 1);
  assert(gw->w.players[1].nb_penguins_placed == 1);
  assert(gw->w.players[1].penguins[0].tile_id == 3);
  assert(gw->w.grid_board.tiles[3].penguin_id == gw->w.players[1].penguins[0].penguin_player_id);
  /*
  assert(gw->w.players[1].penguins[0].score_fishes == gw->w.grid_board.tiles[3].nb_fish);
  assert(gw->w.players[1].score_fishes == gw->w.grid_board.tiles[3].nb_fish);
  assert(gw->w.players[1].penguins[0].score_tiles == 1);
  assert(gw->w.players[1].score_tiles == 1);
  */
  assert(gw->current_player_id == 1);
  assert(gw->current_round == 1);


  game_world__prepare_next_round(gw);
  game_action__do_place(ga, 0, 4);
  game_world__apply_action(gw, ga);
  /*place a third penguin on a tile :
  - The second penguin of the first player is set
  - The first penguin of the first player is still be here
  - The other penguins are not placed
  */
  assert(gw->current_player_id == 0);
  assert(gw->w.players[0].nb_penguins_placed == 2);
  assert(gw->w.players[1].nb_penguins_placed == 1);
  assert(gw->w.players[0].penguins[1].tile_id == 4);
  assert(gw->w.grid_board.tiles[4].penguin_id == gw->w.players[0].penguins[1].penguin_player_id);
  assert(gw->w.players[0].penguins[0].tile_id == 2);
  assert(gw->w.grid_board.tiles[2].penguin_id == gw->w.players[0].penguins[0].penguin_player_id);
  /*
  assert(gw->w.players[0].score_fishes == gw->w.grid_board.tiles[2].nb_fish + gw->w.grid_board.tiles[4].nb_fish);
  assert(gw->w.players[0].penguins[1].score_tiles == 1);
  assert(gw->w.players[0].score_tiles == 2);
  */

  game_world__prepare_next_round(gw);
  game_action__do_place(ga, 1, 5);
  game_world__apply_action(gw, ga);

  game_world__prepare_next_round(gw);
  game_action__do_place(ga, 0, 6);
  game_world__apply_action(gw, ga);

  game_world__prepare_next_round(gw);
  game_action__do_place(ga, 1, 7);
  game_world__apply_action(gw, ga);

  /*place more than nb_peng max per player return an assertion failed
   */

  assert(gw->w.players[0].nb_penguins_placed == 3);
  assert(gw->w.players[1].nb_penguins_placed == 3);

  /*
  assert(gw->w.players[0].score_tiles == 3);
  assert(gw->w.players[1].score_tiles == 3);

  assert(gw->w.players[0].score_distance == 0);
  assert(gw->w.players[1].score_distance == 0);

  assert(gw->w.players[0].score_fishes != 0);
  assert(gw->w.players[1].score_fishes != 0);
  */
  printf("test_place__game_world__apply_action  \t\tréussie\n");
}


void test_move__game_world__apply_action(struct game_world* gw, struct game_action* ga, int nb_players, int nb_penguins_per_player, int nb_tile){

  game_world__prepare_next_round(gw);
  game_action__do_move(ga, 0, 0, 1, 1);
  game_world__apply_action(gw, ga);
  /*move the first penguin of player1 :
   - The penguin is in a new tile
   - There no-penguin in the old tile
   - The other penguins are not moved
   */
  assert(gw->w.players[0].penguins[0].tile_id != 2);
  assert(gw->w.grid_board.tiles[2].penguin_id == -1);
  assert(gw->w.players[0].penguins[1].tile_id == 4);
  assert(gw->w.players[0].penguins[2].tile_id == 6);
  assert(gw->w.players[1].penguins[0].tile_id == 3);
  assert(gw->w.players[1].penguins[1].tile_id == 5);
  assert(gw->w.players[1].penguins[2].tile_id == 7);

  game_world__prepare_next_round(gw);
  game_action__do_move(ga, 1, 0, 1, 1);
  game_world__apply_action(gw, ga);
  //just move the first penguin of player2
  assert(gw->w.players[1].penguins[0].tile_id == 11);
  assert(gw->w.grid_board.tiles[3].penguin_id == -1);
  assert(gw->w.players[1].penguins[1].tile_id == 5);
  assert(gw->w.players[1].penguins[2].tile_id == 7);

  game_world__prepare_next_round(gw);
  game_action__do_move(ga, 0, 1, 1, 1);
  game_world__apply_action(gw, ga);
  //just move the second penguin of player1
  assert(gw->w.players[1].penguins[0].tile_id == 11);
  assert(gw->w.players[0].penguins[1].tile_id == 12);
  assert(gw->w.players[0].penguins[0].tile_id == 10);
  assert(gw->w.grid_board.tiles[4].penguin_id == -1);

  printf("test_move__game_world__apply_action  \t\tréussie\n");


}


int main(int argc, char  **argv) {
  struct game_world gw;
  struct game_action ga;
  int nb_players = 2;
  int nb_penguins_per_player = 3;
  int nb_tiles = 50;
  char *player_names[nb_players];
  player_names[0] = "player1";
  player_names[1] = "player2";

  game_world__init(&gw, player_names, nb_players, nb_penguins_per_player, nb_tiles, TESSEL_HEXA);

  test__game_world__init(&gw,nb_players,nb_penguins_per_player,nb_tiles,player_names);
  test_place__game_world__apply_action(&gw,&ga,nb_players,nb_penguins_per_player,nb_tiles);
  test_move__game_world__apply_action(&gw,&ga,nb_players,nb_penguins_per_player,nb_tiles);

  return EXIT_SUCCESS;
}
