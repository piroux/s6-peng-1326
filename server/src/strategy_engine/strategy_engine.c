
#include "strategy_engine.h"


/* Strategy Engine : Gestionnaire des fonctions d'interface des stratégies.

  Ce composant est responsable de la gestion des fonctions d'interface des
  stratégies.
  Il est l'interface de haut-niveau nécessaire pour pouvoir éxécuter les 3
  méthodes définies obligatoirement dans chaque stratégie.

  Il contient donc un tableau pour stocker les pointeurs de fonction vers les
  fonctions d'interface des stratégies. De plus, c'est le seul composant du
  serveur qui fait appel aux fonctions systèmes POSIX propres à charger et
  décharger du code de bibliothèques partagées (shared objects).
*/


/* Constructeur pour le world_engine */
void strategy_engine__setup(struct strategy_engine *st_eng, struct game_parameters *gp) {
  log_debug("started", NULL);


  st_eng->gp = gp;

  /* */
  st_eng->strategies = malloc(sizeof(struct strategy_client*) * st_eng->gp->nb_strategies);
  if (st_eng->strategies == NULL) {
    perror("import_strategy: malloc failed");
    exit(EXIT_FAILURE);
  }

  /* Importation de chaque stratégie */
  for (int i = 0; i < st_eng->gp->nb_strategies; i++) {
    st_eng->strategies[i] = strategy__import(st_eng->gp->strategies_paths[i]);
  }

  /* Initialisation de chaque stratégie */
  for (int i = 0; i < st_eng->gp->nb_strategies; i++) {
    st_eng->strategies[i]->api_client__setup();
  }

  seed_random(0);

  log_debug("finished", NULL);
}


/* Lanceur d'exécution de stratégie pour un tour */
void strategy_engine__next(struct strategy_engine *st_eng) {
  log_debug("started", NULL);

  /* Exécution de la fonction de calcul du tour pour la stratégie sélectionnée
  dans la structure game_parameters */
  st_eng->strategies[st_eng->gp->current_strategy]->api_client__run();

  log_debug("finished", NULL);
}


/* Destructeur de strategy_engine */
void strategy_engine__cleanup(struct strategy_engine *st_eng) {
  log_debug("started", NULL);

  /* Destruction des stratégie et de leurs pointeurs accès */
  for (int i = 0; i < st_eng->gp->nb_strategies; i++) {
    st_eng->strategies[i]->api_client__cleanup();
    dlclose(st_eng->strategies[i]->handle);
    free(st_eng->strategies[i]);
  }
  free(st_eng->strategies);

  log_debug("finished", NULL);
}
