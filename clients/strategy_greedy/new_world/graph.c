
#include "graph.h"




// Initialize tile
void tile_init(struct tile* t, unsigned int id, unsigned int nb_fish){

    t->id = id;
    t->nb_fish = nb_fish;
    t->id_peng = -1;
    t->id_player = -1;
    t->is_in = 0;

    for (int i = 0; i< MAX_ADJ; i++)
        t->adj[i] = -1;
}



// Construct a graph from interface function
void graph__construct(struct graph* g){

    int nb_dir = 0;
    int i = 0;
    for (i = 0;api_server__is_free(i) != -1 ; i++) {
        struct tile* t = malloc(sizeof(struct tile));
        tile_init(t,i,api_server__nb_fish(i));
        if (i == 0){
            g->tiles = t;
        }
        nb_dir = api_server__nb_directions(i);
        if (nb_dir != 0)
            t->is_in = 1;
        for (int j = 0; j<nb_dir; j++) {
            unsigned int type = api_server__direction_type(i,j);
            t->adj[type] = api_server__get_tile(i, type, 1);
        }
        t++;
    }
    g->nb_tiles = i;
}



//Calculate the total number of tiles in the graph
int nb_tiles_total (){
  int nb_tile = 0;

  while(api_server__is_free(nb_tile) || api_server__is_free(nb_tile+1))
    nb_tile++;

  return nb_tile;
}
