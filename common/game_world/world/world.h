
#ifndef  __WORLD_H__
#define  __WORLD_H__

#include "board/grid.h"
#include "board/tessel.h"
#include "player.h"
#include "world_conf.h"


struct world {
  /* --- Objets du modèle --- */

  /* Ensemble des joueurs */
  struct player players[MAX_PLAYERS];

  /* Plateau de jeu */
  struct grid grid_board;


  /* --- Configuration du modèle --- */

  /* Nombre de joueurs */
  int nb_players;

  /* Nombre de pingouins par joueur */
  int nb_penguins_per_player;
};


void world__init (struct world *w, char **player_names, int nb_players, int nb_penguins_per_player,  int nb_tiles, enum tessel_kind tk);
void world__copy (struct world *w_dst, struct world *w_src);
void world__destroy (struct world *w);


#endif
