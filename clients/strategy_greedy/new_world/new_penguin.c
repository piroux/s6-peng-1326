
#include "new_penguin.h"

//construct structure of penguin from interface functions
void penguin__construct (struct graph* g, struct penguin *pg, unsigned int player_id, unsigned int penguin_player_id) {
    
    //initialize penguin structure
    pg->player_id = player_id;
    pg->tile_id = api_server__get_penguin_tile(player_id,penguin_player_id);
    
    //add the penguin on the new graph
    g->tiles[pg->tile_id].id_player = player_id;
    g->tiles[pg->tile_id].id_peng = penguin_player_id;
}

