

#ifndef  __TCOORD_H__
#define  __TCOORD_H__

#include <stdio.h>


/* Tile coord */
struct tcoord {
  int x, y;
  char s[(4*sizeof(unsigned int))*2 + 2]; // "xxxx:xxxx\0"
};


void tcoord__set(struct tcoord *c, int x, int y);
void tcoord__copy(struct tcoord *c_dst, struct tcoord *c_src);
void tcoord__diff(struct tcoord *c_diff, struct tcoord *c2, struct tcoord *c1);

#endif
