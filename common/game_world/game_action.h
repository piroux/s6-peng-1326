
#ifndef  __GAME_ACTION_H__
#define  __GAME_ACTION_H__

#include <stdio.h>


/* Types possibles d'action */
enum action_kind {
  ACTION_PLACE,   /* During the first rounds. */
  ACTION_MOVE,    /* When all penguins are placed and are not blocked. */
  ACTION_NOP      /* Only if all penguins are blocked. */
};


struct game_action {

  unsigned int id;

  /* Stratégie : seulement utile pour la sérialisation. */
  int player_id;

  /* Compteur d'avertissements levés dans les fonctions de l'interface */
  unsigned int warnings;

  /* Compteur du nombre d'actions demandées durant le dernier tour */
  unsigned int count;

  /* Type d'action */
  enum action_kind kind;

  /* Paramères d'action nécessaires au modèle de jeu */
  union params {
    struct place_params {
      unsigned int tile_id;
    } place_params;
    struct move_params {
      unsigned int penguin_player_id;
      unsigned int direction;
      unsigned int distance;
    } move_params;
  } params;
};


void game_action__init(struct game_action *ga);
void game_action__reset(struct game_action *ga);

void game_action__print(struct game_action *ga);

void game_action__do_place(struct game_action *ga,  unsigned int player_id, unsigned int tile_id);
void game_action__do_move(struct game_action *ga,  unsigned int player_id, unsigned int penguin_id, unsigned int direction, unsigned int distance);



#endif
