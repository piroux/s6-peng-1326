#ifndef __INTERFACE_STRATEGY_SERVER_H__
#define __INTERFACE_STRATEGY_SERVER_H__

/** STRUCTURE
 *
 * Ce fichier interface.h correspond à une liste exhaustive de fonctions permettant à toute stratégie de jouer au jeu de Pingouin.
 * Aucun pointeur vers des données du jeu n'est requis, ce qui diminue fortement le risque de triche
 * En contrepartie, le fichier interface.c qui implémente les fonctions ci présentes requiert d'accéder
 * aux données du jeu qui devront alors nécessairement être globales.
 *
 */

/** REGLES DE BASE
 *
 * -A chaque case du plateau est associé un entier (facile à manipuler)
 * Cet entier peut être attribué de façon tout à fait arbitraire, cela ne change rien pour la stratégie
 * du moment que deux cases aient un identifiant différent
 *
 * -Les cases du plateau pouvant être de n'importe quelle forme, les directions seront indexées par un entier
 * dans [0, n[, avec n le résultat de __nb_directions appliquée à l'identifiant de la case en question
 *
 * -Les seules fonctions permettant d'obtenir des identifiants de case sont __get_penguin_tile, qui retourne
 * l'identifiant d'une case ou se trouve un pingouin, __get_tile, qui retourne l'identifiant d'une case
 * depuis une autre case dans une certaine direction, et __get_starting_tile, qui retourne l'identifiant
 * d'une case où placer un pingouin au début du jeu
 *
 * -Une conséquence de ces choix est que la stratégie n'a pas à savoir ce qu'est une ligne droite, et n'a
 * qu'à utiliser __get_tile pour accéder à de nouvelle cases en ligne droite depuis une case donnée.
 *
 * -Les seules fonctions permettant de donner une consigne au serveur sont __place_penguin et __move_penguin
 *
 * [0]
 * La fonction __nb_penguins n'est pas nécessaires si l'on ne se place que dans
 * les conditions d'un jeu de Pingouins classique (4 joueurs -> 2 pingouins, 2 joueurs -> 4 pingouins)
 * Il nous a semblé utile de pouvoir changer ce paramètre pour facilement diversifier le jeu.
 * Ce choix est arbitraire et donc tout à fait discutable.
 *
 * [1]
 * Les deux dernieres fonctions servent à obtenir facilement les cases de depart. En effet, au début du
 * jeu, les stratégies n'ont connaissance de l'identifiant d'aucune case, et doivent donc avoir un
 * moyen d'y acceder.
 *
 * [2]
 * Il est possible qu'un serveur ne donne pas le même nombre de pingouins à tous les joueurs.
 */


#define __INTERFACE_STRATEGY_SERVER_VERSION__ 3


//Retoune le nombre de joueurs
unsigned int api_server__nb_players ();

//Retourne l'identifiant du joueur courant
unsigned int api_server__current_player ();

//Retoune l'identifiant de la case sur laquelle se trouve le pingouin d'un joueur
unsigned int api_server__get_penguin_tile (unsigned int player_id, unsigned int penguin_id);

//Retourne le nombre de poissons sur une case
int api_server__nb_fish (unsigned int tile_id);

//Retourne le nombre de cases accessibles depuis une case donnée
unsigned int api_server__nb_directions (unsigned int tile_id);

//Retourne l'identifiant de la case se trouvant en ligne droite d'un certain nombre de cases dans une certaine direction depuis une case donnée.
//Si une telle case n'existe pas, retourne un identifiant tel que la fonction__is_free appliquée à cet identifiant renvoit 0 (pas libre)
unsigned int api_server__get_tile (unsigned int tile_id, unsigned int direction, unsigned int distance);

//Retourne 1 si la case est libre (pas de pingouins dessus et case appartenant au plateau), 0 sinon
int api_server__is_free (unsigned int tile_id);

//Place un pingouin sur une case (debut de partie)
int api_server__place_penguin (unsigned int tile_id);

//Réalise l'action du joueur: demander au serveur de bouger un pingouin
int api_server__move_penguin (unsigned int penguin_id, unsigned int direction, unsigned int distance);

//Conversion d'un identifiant d'un côté d'une case en direction globale
unsigned int api_server__direction_type (unsigned int tile_id, unsigned int direction);

//Indique le nombre de poissons collectés
int api_server__player_score_fish (unsigned int player_id);

//Indique le nombre de cases collectées
int api_server__player_score_tiles (unsigned int player_id);

//Retourne le nombre maximal de pingouins pour un joueur (cf [2])
unsigned int api_server__nb_penguins (unsigned int player_id);

//Retourne le nombre de pingouins placés sur le plateau par un joueur (cf [0])
unsigned int api_server__nb_penguins_placed (unsigned int player_id);

//Retourne le nombre de case de départ valides (cf [1])
unsigned int api_server__nb_starting_tiles ();

//Retourne l'identifiant de la nième case de départ valide (cf [1])
unsigned int api_server__get_starting_tile (unsigned int starting_tile_id);

//Retourne la version de l'interface implémentée par le serveur
int api_server__version ();

#endif
