
#include "game_action.h"


/* Game Action : Modèle d'action à appliquer au modèle de jeu

  Ce composant repésente l'action enregistrée lors de l'appel des fonctions
	de l'interface. Après son enregistrement, celle-ci devra être vérifiée par le
	modèle de jeu. Si celle-ci est valide, elle pourra être appliquée au modèle de
	jeu.

	Il a été choisi d'utiliser une structure de données versatile commune afin
	de faciliter la gestion de l'application des actions au modèle de jeu
	game_world.
	Cela est rendu possible par le fait qu'il n'y ait que deux actions possibles
	définies dans l'interface. S'il avait fallut implémenter une dizaine d'actions
	différentes, un autre modèle de représentation des actions aurait certainement
	été choisi.

	Le modèle de game_action contient donc :
		- le type de l'action,
		- les paramètres de l'actionen question.
		- le nombre d'actions demandées par la stratégie,
*/


/* Initialisateur de game_action */
void game_action__init(struct game_action *ga) {
	ga->id = 0;
	ga->player_id = -1;
	ga->warnings = 0;
	ga->count = 0;
	ga->kind = ACTION_NOP;
}


/* Réinitialisateur de game_action */
void game_action__reset(struct game_action *ga) {
	ga->id++;
	ga->player_id = -1;
	ga->warnings = 0;
	ga->count = 0;
	ga->kind = ACTION_NOP;
}


/* Enregistreur d'action de placement de pingouin (place) */
void game_action__do_place(struct game_action *ga,  unsigned int player_id, unsigned int tile_id) {
	ga->count++;
	ga->player_id = player_id;
	ga->kind = ACTION_PLACE;
	ga->params.place_params.tile_id = tile_id;
}


/* Enregistreur d'action de placement de pingouin (move) */
void game_action__do_move(struct game_action *ga, unsigned int player_id, unsigned int penguin_id, unsigned int direction, unsigned int distance) {
	ga->count++;
	ga->player_id = player_id;
	ga->kind = ACTION_MOVE;
	ga->params.move_params.penguin_player_id = penguin_id;
	ga->params.move_params.direction = direction;
	ga->params.move_params.distance = distance;
}


/* Afficheur de game_action */
void game_action__print(struct game_action *ga) {
	fprintf(stderr, "game_action: \n");
	fprintf(stderr, "	* id = %u\n", ga->id);
	fprintf(stderr, "	* player_id = %u\n", ga->player_id);
	fprintf(stderr, "	* warnings = %u\n", ga->warnings);
	fprintf(stderr, "	* count = %u\n", ga->count);
	switch (ga->kind) {
		case ACTION_PLACE:
			fprintf(stderr, "	* kind = PLACE\n");
			fprintf(stderr, "	* param: tile_id = %u\n", ga->params.place_params.tile_id);
			break;
		case ACTION_MOVE:
			fprintf(stderr, "	* kind = MOVE\n");
			fprintf(stderr, "	* param: penguin_player_id = %u\n", ga->params.move_params.penguin_player_id);
			fprintf(stderr, "	* param: direction_id = %u\n", ga->params.move_params.direction);
			fprintf(stderr, "	* param: distance = %u\n", ga->params.move_params.distance);
			break;
		default:
			fprintf(stderr, "This kind of action is not implemented (%d)!\n", ga->kind);
			break;
	}
}
