
#ifndef ____place_fonction__
#define ____place_fonction__

#include <stdio.h>
#include <assert.h>

#include <string.h>
#include <stdlib.h>

#include "interface_strategy_server.h"
#include "interface_strategy_client.h"

#define MAX_TILES 50
#define MAX_INTERESTING_TILE 1
#define DEPTH 0

struct interesting_tile{
    unsigned int id;
    
    //how our penguin can move into this tile
    unsigned int from_dir;
    unsigned int from_dist;

    //coefficient which determinate the more interesting tile 
    int score_fish;
    int score_adv;
};


void  interesting_tile__init (struct interesting_tile* t,unsigned int id, unsigned int dist, unsigned int type);


void access_tiles_n_fish (unsigned int peng_tile, int nb_dir, int nb_fish,  struct interesting_tile** interesting_tiles);

void access_tile_adversaries (int nb_dir, int nb_peng,unsigned int current_player, struct interesting_tile* t);

void sort_best_move (struct interesting_tile* t);

struct interesting_tile best_move_rec (unsigned int current_player, unsigned int peng_tile, int nb_dir, int nb_peng, int depth);

struct interesting_tile best_move (unsigned int current_player, unsigned int peng_tile, int nb_dir, int nb_peng);

int move();
#endif /* defined(____place_fonction__) */

