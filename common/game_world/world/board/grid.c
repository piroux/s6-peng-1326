
#include "grid.h"


void grid__init (struct grid *gd, unsigned int nb_tiles, enum tessel_kind tk) {

  gd->nb_tiles = nb_tiles;

  tessel__init(&gd->tessel_grid, tk, nb_tiles);

  for(int i = 0; i < nb_tiles; i++) {
    tile__init(&gd->tiles[i], &gd->tessel_grid);
  }

  gd->graph = MBgraph2_create();

  for(int i = 0; i < nb_tiles; i++) {
    MBgraph2_add(gd->graph, gd->tiles[i].coord.s, &gd->tiles[i]);
    //fprintf(stderr, "added vertex[%3d]: (%s)\n", gd->tiles[i].id, gd->tiles[i].coord.s);
  }

  struct tcoord c;
  int created_edges = 0; // debug
  for(int i = 0; i < nb_tiles; i++) {
    MBvertex *tile_vertex1 = MBgraph2_get_vertex(gd->graph, gd->tiles[i].coord.s);
    assert(tile_vertex1 != NULL);
    for (int d = 0; d < gd->tessel_grid.nb_directions; d++) {
      gd->tessel_grid.set_tcoord_for_dir(&gd->tessel_grid,
        &c,
        &gd->tiles[i].coord,
        d);
      if (c.x < 0 || c.y < 0
          || c.x >= gd->tessel_grid.tiles_per_axis
          || c.y >= gd->tessel_grid.tiles_per_axis) { // TODO: find more exact limit for y
        continue;
      }
      else {
        MBvertex *tile_vertex2 = MBgraph2_get_vertex(gd->graph, c.s);
        if (tile_vertex2 != NULL) {
          MBgraph2_add_edge(gd->graph, tile_vertex1, tile_vertex2);
          MBgraph2_add_edge(gd->graph, tile_vertex2, tile_vertex1);
          //fprintf(stderr, "added undirected edge\n");
          created_edges++;
        }
      }
    }
  }
}


void grid__copy (struct grid *gd_dst, struct grid *gd_src) {

  gd_dst->nb_tiles = gd_src->nb_tiles;

  tessel__copy(&gd_dst->tessel_grid, &gd_src->tessel_grid);
  //tessel__init(&gd_dst->tessel_grid, gd_src->tessel_grid.kind, gd_src->nb_tiles);

  MBgraph2_delete(gd_dst->graph);
  gd_dst->graph = MBgraph2_create();

  // Vertices are never deleted during the game, only the edges.
  for(int i = 0; i < gd_src->nb_tiles; i++) {
    tile__copy(&gd_dst->tiles[i], &gd_src->tiles[i]);
    MBgraph2_add(gd_dst->graph, gd_dst->tiles[i].coord.s, &gd_dst->tiles[i]);
  }

  MBiterator *edges;
  MBedge *edge;
  edges = MBgraph2_get_edges(gd_src->graph);
  while ((edge = MBiterator_get(edges))) {
    //MBvertex_get_name(MBedge_get_from(edge));
    const char *name1 = MBvertex_get_name(MBedge_get_from(edge));
    //fprintf(stderr, "found name src: %s (%p)\n", name1, name1);
    MBvertex *v1 = MBgraph2_get_vertex(gd_dst->graph, name1);
    assert(v1 != NULL);
    const char *name2 = MBvertex_get_name(MBedge_get_to(edge));
    //fprintf(stderr, "found name src: %s (%p)\n", name1, name1);
    MBvertex *v2 = MBgraph2_get_vertex(gd_dst->graph, name2);
    assert(v2 != NULL);
    MBgraph2_add_edge(gd_dst->graph, v1, v2);
  }
  MBiterator_delete(edges);
}


void grid__destroy (struct grid *gd) {
  for(int i = 0; i < gd->nb_tiles; i++) {
    tile__destroy(&gd->tiles[i]);
  }

  MBgraph2_delete(gd->graph);
}


void grid__print(struct grid *gd) {
  MBgraph2 *graph = gd->graph;

  fprintf(stderr, "Vertices=%d Edges=%d\n", MBgraph2_get_vertex_count(graph), MBgraph2_get_edge_count(graph));

  MBiterator *vertices;
  MBvertex *vertex;

  vertices = MBgraph2_get_vertices(graph);
  while ((vertex = MBiterator_get(vertices))) {
    MBiterator *neighbours;
    MBvertex *neighbour;
    unsigned int n = 0;
    struct tile* vtile = MBvertex_get_data(vertex);
    assert(vtile != NULL);
    assert(strcmp(MBvertex_get_name(vertex), vtile->coord.s) == 0);
    fprintf(stderr, " * %s[%2u]{%d} (%d)-> ", MBvertex_get_name(vertex), vtile->id, vtile->state, MBgraph2_get_neighbour_count(graph, vertex));
    neighbours = MBgraph2_get_neighbours(graph, vertex);
    while ((neighbour = MBiterator_get(neighbours))) {
      struct tile* ntile = MBvertex_get_data(neighbour);
      assert(ntile != NULL);
      fprintf(stderr, "%s[%2u]%u{%d} ",
        MBvertex_get_name(neighbour),
        ntile->id, gd->tessel_grid.get_global_dir(&gd->tessel_grid, &ntile->coord, &vtile->coord),
        ntile->state);
      if (n < MBgraph2_get_neighbour_count(graph, vertex) - 1) {
        fprintf(stderr, ", ");
      }
      n++;
    }
    fprintf(stderr, "\n");
    MBiterator_delete(neighbours);
  }
  fprintf(stderr, "\n");
  MBiterator_delete(vertices);
  /*
  MBiterator *edges;
  MBedge *edge;

  //fprintf(stderr, "Edges (%d):\n\n", MBgraph2_get_edge_count(graph));
  edges = MBgraph2_get_edges(graph);
  while ((edge = MBiterator_get(edges))) {
      fprintf(stderr, "<%s, %s>\n", MBvertex_get_name(MBedge_get_from(edge)), MBvertex_get_name(MBedge_get_to(edge)));
  }
  fprintf(stderr, "\n");
  */
}


unsigned int grid__nb_starting_tiles (struct grid *gd) {
  unsigned int nb_starting_tiles = 0;
  for (int tile_id = 0; tile_id < gd->nb_tiles; tile_id++) {
    if (gd->tiles[tile_id].state == FREE) {
      nb_starting_tiles++;
    }
  }
  return nb_starting_tiles;
}


unsigned int grid__nb_free_neighbours (struct grid *gd, unsigned int tile_id) {
  assert(tile_id < gd->nb_tiles);

  struct tcoord tc;
  gd->tessel_grid.init_tcoord(&gd->tessel_grid, tile_id, &tc);

  MBvertex *vx1 = MBgraph2_get_vertex(gd->graph, tc.s);
  assert(vx1 != NULL);

  int nb_free_neighbours = 0;

  MBiterator *neighbours;
  MBvertex *vx2;
  neighbours = MBgraph2_get_neighbours(gd->graph, vx1);
  while ((vx2 = MBiterator_get(neighbours))) {
    assert(vx2 != NULL);
    struct tile *t2 = MBvertex_get_data(vx2);
    if (t2->state == FREE) {
      nb_free_neighbours++;
    }

  }
  MBiterator_delete(neighbours);

  return nb_free_neighbours;
}


unsigned int grid__get_first_free_neighbour (struct grid *gd, unsigned int tile_id) {
  assert(tile_id < gd->nb_tiles);

  struct tcoord tc;
  gd->tessel_grid.init_tcoord(&gd->tessel_grid, tile_id, &tc);

  MBvertex *vx1 = MBgraph2_get_vertex(gd->graph, tc.s);
  assert(vx1 != NULL);


  int neighbour_id = -1;

  MBiterator *neighbours;
  MBvertex *vx2;
  neighbours = MBgraph2_get_neighbours(gd->graph, vx1);
  while ((vx2 = MBiterator_get(neighbours)) && neighbour_id < 0) {
    assert(vx2 != NULL);
    struct tile *t2 = MBvertex_get_data(vx2);
    if (t2->state == FREE) {
      neighbour_id = t2->id;
    }

  }
  MBiterator_delete(neighbours);

  return neighbour_id;
}

int grid__has_free_neighbours (struct grid *gd, unsigned int tile_id) {
  assert(tile_id < gd->nb_tiles);

  struct tcoord tc;
  gd->tessel_grid.init_tcoord(&gd->tessel_grid, tile_id, &tc);

  MBvertex *vx1 = MBgraph2_get_vertex(gd->graph, tc.s);
  assert(vx1 != NULL);

  int has_free_neighbours = 0;

  MBiterator *neighbours;
  MBvertex *vx2;
  neighbours = MBgraph2_get_neighbours(gd->graph, vx1);
  while ((vx2 = MBiterator_get(neighbours)) && !has_free_neighbours) {
    assert(vx2 != NULL);
    struct tile *t2 = MBvertex_get_data(vx2);
    if (t2->state == FREE) {
      has_free_neighbours = 1;
    }

  }
  MBiterator_delete(neighbours);

  return has_free_neighbours;
}


unsigned int grid__get_starting_tile (struct grid *gd, unsigned int starting_tile_id) {
  assert(starting_tile_id < grid__nb_starting_tiles(gd));

  unsigned int tile_id = 0;
  int goal_start_id = starting_tile_id;
  int next_start_id = 0;
  for (tile_id = 0; next_start_id < (goal_start_id + 1) && tile_id < gd->nb_tiles; tile_id++) {
    if (gd->tiles[tile_id].state == FREE) {
      next_start_id++;
    }
  }

  return tile_id - 1;
}


unsigned int grid__nb_directions (struct grid *gd, unsigned int tile_id) {
  assert(tile_id < gd->nb_tiles);

  MBvertex *v = MBgraph2_get_vertex(
    gd->graph,
    gd->tiles[tile_id].coord.s);

  assert(v != NULL);

  return MBgraph2_get_neighbour_count(
    gd->graph,
    v);
}


unsigned int grid__get_global_dir (struct grid *gd, unsigned int tile_id, unsigned int direction) {
  /* direction is an available direction */
  assert(tile_id < gd->nb_tiles);
  assert(grid__nb_directions(gd, tile_id));

  struct tcoord tc;
  gd->tessel_grid.init_tcoord(&gd->tessel_grid, tile_id, &tc);

  MBvertex *vx1, *vx2;
  vx1 = MBgraph2_get_vertex(gd->graph, tc.s);
  assert(vx1 != NULL);

  MBiterator *edges;
  edges = MBgraph2_get_neighbours(gd->graph, vx1);
  unsigned int direction_available_id = 0;
  while ((vx2 = MBiterator_get(edges))) {
    if (direction_available_id != direction) {
      direction_available_id++;
      continue;
    }
    else {
      break;
    }
  }
  MBiterator_delete(edges);

  assert(vx1 != NULL);
  assert(vx2 != NULL);
  struct tile *t1 = MBvertex_get_data(vx1);
  struct tile *t2 = MBvertex_get_data(vx2);

  unsigned int global_dir = gd->tessel_grid.get_global_dir(&gd->tessel_grid, &t2->coord, &t1->coord);

  log_trace("global direction found for %u: %s[%2u]{%d} -> %s[%2u]{%d} = %u",
    direction,
    MBvertex_get_name(vx1), t1->id, t1->state,
    MBvertex_get_name(vx2), t2->id, t2->state,
    global_dir);

  return global_dir;
  //return gd->tessel_grid.get_global_dir(&gd->tessel_grid, &t1->coord, &t2->coord);
}


void grid__normalize_tiles (struct grid *gd) {
  MBgraph2 *graph = gd->graph;
  MBiterator *vertices;
  MBvertex *vertex;

  vertices = MBgraph2_get_vertices(graph);

  while ((vertex = MBiterator_get(vertices))) {
    MBiterator *neighbours;
    MBvertex *neighbour;

    struct tile* vtile = MBvertex_get_data(vertex);
    assert(vtile != NULL);
    assert(strcmp(MBvertex_get_name(vertex), vtile->coord.s) == 0);

    if (vtile->state == DEAD && MBgraph2_get_neighbour_count(graph, vertex) > 0) {
      neighbours = MBgraph2_get_neighbours(graph, vertex);
      while ((neighbour = MBiterator_get(neighbours))) {
        struct tile* ntile = MBvertex_get_data(neighbour);
        assert(ntile != NULL);

        MBgraph2_remove_edge(graph, vertex, neighbour);
        MBgraph2_remove_edge(graph, neighbour, vertex);

        log_trace("removed edge: %s[%2u]{%d} <-> %s[%2u]{%d}",
          MBvertex_get_name(vertex), vtile->id, vtile->state,
          MBvertex_get_name(neighbour), ntile->id, ntile->state);
      }
      MBiterator_delete(neighbours);
    }
  }
  MBiterator_delete(vertices);
}


void grid__takeoff_tile (struct grid *gd, unsigned int tile_id) {
  assert(tile_id < gd->nb_tiles);

  gd->tiles[tile_id].penguin_id = -1;
  gd->tiles[tile_id].state = DEAD;

  struct tcoord tc;
  gd->tessel_grid.init_tcoord(&gd->tessel_grid, tile_id, &tc);

  MBvertex *vx1 = MBgraph2_get_vertex(gd->graph, tc.s);
  assert(vx1 != NULL);
  struct tile *t1 = MBvertex_get_data(vx1);
  assert(t1 != NULL);

  MBiterator *neighbours;
  MBvertex *vx2;
  neighbours = MBgraph2_get_neighbours(gd->graph, vx1);
  while ((vx2 = MBiterator_get(neighbours))) {
    assert(vx2 != NULL);
    MBgraph2_remove_edge(gd->graph, vx1, vx2);
    MBgraph2_remove_edge(gd->graph, vx2, vx1);

    struct tile *t2 = MBvertex_get_data(vx2);
    assert(t2 != NULL);
    /*
    if (t2->state != DEAD && MBgraph2_get_neighbour_count(gd->graph, vx2) == 0) {
      log_trace("make dead : %s[%2u]{%d}", MBvertex_get_name(vx2), t2->id, t2->state);
      t2->state = DEAD;
    }
    */

    log_trace("removed edge: %s[%2u]{%d} <-> %s[%2u]{%d}",
      MBvertex_get_name(vx1), t1->id, t1->state,
      MBvertex_get_name(vx2), t2->id, t2->state);

  }
  MBiterator_delete(neighbours);

  grid__normalize_tiles(gd);
}


void grid__landing_tile (struct grid *gd, unsigned int tile_id, unsigned int penguin_id) {
  gd->tiles[tile_id].state = TAKEN;
  gd->tiles[tile_id].penguin_id = penguin_id;
}


int grid__validate_path (struct grid *gd, unsigned int starting_tile_id, unsigned int direction, unsigned int distance) {
  //printf("%s: starting_tile_id=%u, direction=%u, distance=%u\n", __func__, starting_tile_id, direction, distance);
  log_trace("starting_tile_id=%u, direction=%u, distance=%u",starting_tile_id, direction, distance);
  assert(starting_tile_id < gd->nb_tiles);
  assert(direction < gd->tessel_grid.nb_directions);

  struct tile* starting_tile = &gd->tiles[starting_tile_id];
  struct tcoord tc1, tc2;
  MBvertex *vx1, *vx2;
  struct tile *next_tile = starting_tile;
  unsigned int next_tile_id = starting_tile_id;

  for (int d = 0; d < distance; d++) {
    /* Testing the next hop in the path. */
    gd->tessel_grid.init_tcoord(&gd->tessel_grid, next_tile_id, &tc1);
    gd->tessel_grid.set_tcoord_for_dir(&gd->tessel_grid, &tc2, &tc1, direction);
    // no check here !! do them elsewhere !!
    vx1 = MBgraph2_get_vertex(gd->graph, tc1.s);
    vx2 = MBgraph2_get_vertex(gd->graph, tc2.s);

    assert(vx1 != NULL);
    if (vx2 == NULL) {
      /* The path exits the board. */
      log_trace("=> not in graph vx2 : %s", tc2.s);
      return -1;
    }

    next_tile = MBvertex_get_data(vx2);
    assert(next_tile != NULL);
    next_tile_id = next_tile->id;

    if (!MBgraph2_get_adjacent(gd->graph, vx1, vx2)) {
      /* The neighbours vx1 and vx2 are not connected.
        It is pretty sure that vx2 is a dead tile. */
        log_trace("=> non adjacent vx2 : %s[%2u]{%d}",
          MBvertex_get_name(vx2), next_tile->id, next_tile->state);
      return -2;
    }

    if (next_tile->state != FREE) {
      //assert(next_tile->state == TAKEN);
      /* This tile is already taken by another penguin. */
      log_trace("=> vx2 not free : %s[%2u]{%d}",
        MBvertex_get_name(vx2), next_tile->id, next_tile->state);
      return -3;
    }

    log_trace("free and valid vx2 : %s[%2u]{%d}",
      MBvertex_get_name(vx2), next_tile->id, next_tile->state);
  }

  return next_tile_id;
}
