#!/bin/bash

set -o errexit
set -x


rm -rf build && mkdir build && cd build && cmake .. && make && make test
