
#include "tessel.h"


void tessel__init(struct tessel *t, enum tessel_kind tk, unsigned int nb_tiles) { // tessel factory
  t->kind = tk;
  switch (tk) {
    case TESSEL_SQUARE:
      t->nb_directions = 4;
      t->tiles_per_axis = ((unsigned int) sqrt((float) nb_tiles)) + 1;
      t->init_tcoord = tessel_square__init_tcoord;
      t->set_tcoord_for_dir = tessel_square__set_tcoord_for_dir;
      t->get_global_dir = tessel_square__get_global_dir;
      break;
    case TESSEL_HEXA:
      t->nb_directions = 6;
      t->tiles_per_axis = ((unsigned int) sqrt((float) nb_tiles)) + 1;
      t->init_tcoord = tessel_hexa__init_tcoord;
      t->set_tcoord_for_dir = tessel_hexa__set_tcoord_for_dir;
      t->get_global_dir = tessel_hexa__get_global_dir;
      break;
    default:
      break;
  }
}


void tessel__copy(struct tessel *t_dst, struct tessel *t_src) {
  t_dst->kind = t_src->kind;
  t_dst->nb_directions = t_src->nb_directions;
  t_dst->tiles_per_axis = t_src->tiles_per_axis;

  t_dst->init_tcoord = t_src->init_tcoord;
  t_dst->set_tcoord_for_dir = t_src->set_tcoord_for_dir;
  t_dst->get_global_dir = t_src->get_global_dir;
}


/* Square tessel */

void tessel_square__init_tcoord(struct tessel *t, unsigned int tile_id, struct tcoord *c) {
  // usual coordinates for square grid
  unsigned int x = tile_id % t->tiles_per_axis;
  unsigned int y = tile_id / t->tiles_per_axis;
  tcoord__set(c, x, y);
}


void tessel_square__set_tcoord_for_dir(struct tessel *t, struct tcoord *tc2, struct tcoord *tc1, unsigned int direction) {
  switch (direction) {
    case 0:
      tcoord__set(tc2, tc1->x + 1, tc1->y);
      break;
    case 1:
      tcoord__set(tc2, tc1->x, tc1->y + 1);
      break;
    case 2:
      tcoord__set(tc2, tc1->x - 1, tc1->y);
      break;
    case 3:
      tcoord__set(tc2, tc1->x, tc1->y - 1);
      break;
    default:
      perror("tessel_square__set_tcoord_for_dir: direction not recognized.");
      exit(-1);
  }
}

unsigned int tessel_square__get_global_dir(struct tessel *t, struct tcoord *tc2, struct tcoord *tc1) {
  struct tcoord tcd; /* for tcoord diff */
  tcoord__diff(&tcd, tc2, tc1);
  assert(tcd.x == -1 || tcd.x == 0 || tcd.x == 1);
  assert(tcd.y == -1 || tcd.y == 0 || tcd.y == 1);
  assert(tcd.x == 0 || tcd.y == 0);

  int global_dir = -1;

  /* Multiple nested 'if' are much more readable than nested 'switch'. */
  if (tcd.x == 0) {
    if (tcd.y == -1) {
      global_dir = 3;
    }
    else if (tcd.y == 1) {
      global_dir = 1;
    }
  }
  else if (tcd.y == 0) {
    if (tcd.x == -1) {
      global_dir = 2;
    }
    else if (tcd.x == 1) {
      global_dir = 0;
    }
  }

  assert(global_dir >= 0 && global_dir < t->nb_directions);

  return global_dir;
}


/* Hexa tessel */

void tessel_hexa__init_tcoord(struct tessel *t, unsigned int tile_id, struct tcoord *c) {
  // offset coordinates for hexa grid
  unsigned int x = tile_id % t->tiles_per_axis;
  unsigned int y = tile_id / t->tiles_per_axis;
  tcoord__set(c, x, y);
}


void tessel_hexa__set_tcoord_for_dir(struct tessel *t, struct tcoord *tc2, struct tcoord *tc1, unsigned int direction) {
  if (tc1->y % 2 == 0) {
    switch (direction) {
      case 0:
        tcoord__set(tc2, tc1->x + 1, tc1->y);
        break;
      case 1:
        tcoord__set(tc2, tc1->x, tc1->y + 1);
        break;
      case 2:
        tcoord__set(tc2, tc1->x - 1, tc1->y + 1);
        break;
      case 3:
        tcoord__set(tc2, tc1->x - 1, tc1->y);
        break;
      case 4:
        tcoord__set(tc2, tc1->x - 1, tc1->y - 1);
        break;
      case 5:
        tcoord__set(tc2, tc1->x, tc1->y - 1);
        break;
      default:
        perror("tessel_square__set_tcoord_for_dir: direction not recognized.");
        exit(-1);
    }
  }
  else {
    switch (direction) {
      case 0:
        tcoord__set(tc2, tc1->x + 1, tc1->y);
        break;
      case 1:
        tcoord__set(tc2, tc1->x + 1, tc1->y + 1);
        break;
      case 2:
        tcoord__set(tc2, tc1->x, tc1->y + 1);
        break;
      case 3:
        tcoord__set(tc2, tc1->x - 1, tc1->y);
        break;
      case 4:
        tcoord__set(tc2, tc1->x, tc1->y - 1);
        break;
      case 5:
        tcoord__set(tc2, tc1->x + 1, tc1->y - 1);
        break;
      default:
        perror("tessel_square__set_tcoord_for_dir: direction not recognized.");
        exit(-1);
    }
  }
}


unsigned int tessel_hexa__get_global_dir(struct tessel *t, struct tcoord *tc2, struct tcoord *tc1) {
  struct tcoord tcd; /* for tcoord diff */
  tcoord__diff(&tcd, tc2, tc1);
  assert(tcd.x == -1 || tcd.x == 0 || tcd.x == 1);
  assert(tcd.y == -1 || tcd.y == 0 || tcd.y == 1);

  int global_dir = -1;

  /* Multiple nested 'if' are much more readable than nested 'switch'. */
  if (tc1->y % 2 == 0) {
    if (tcd.x ==  1 && tcd.y ==  0) {
      global_dir = 0;
    }
    else if (tcd.x ==  0 && tcd.y ==  1) {
      global_dir = 1;
    }
    else if (tcd.x == -1 && tcd.y ==  1) {
      global_dir = 2;
    }
    else if (tcd.x == -1 && tcd.y ==  0) {
      global_dir = 3;
    }
    else if (tcd.x == -1 && tcd.y == -1) {
      global_dir = 4;
    }
    else if (tcd.x ==  0 && tcd.y == -1) {
      global_dir = 5;
    }
    /* else error : handled at the end of the function. */
  }
  else {
    if (tcd.x ==  1 && tcd.y ==  0) {
      global_dir = 0;
    }
    else if (tcd.x ==  1 && tcd.y ==  1) {
      global_dir = 1;
    }
    else if (tcd.x ==  0 && tcd.y ==  1) {
      global_dir = 2;
    }
    else if (tcd.x == -1 && tcd.y ==  0) {
      global_dir = 3;
    }
    else if (tcd.x ==  0 && tcd.y == -1) {
      global_dir = 4;
    }
    else if (tcd.x ==  1 && tcd.y == -1) {
      global_dir = 5;
    }
    /* else error : handled at the end of the function. */
  }

  assert(global_dir >= 0 && global_dir < t->nb_directions);

  return global_dir;
}
