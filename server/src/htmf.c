
#include "game_engine/game_engine.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <libgen.h>


int check_param_sharedlib(char *pathlib) {
  return (access(pathlib, F_OK) != -1);
}


int main(int argc, char **argv) {

  struct game_parameters gp;

  int nb_strategies = 0;
  char *strategies[256];

  for (int i = 1; i < argc; i++ ) {
    if (check_param_sharedlib(argv[i])) {
      //strategies[nb_strategies] = basename(argv[i]);
      strategies[nb_strategies] = argv[i];
      nb_strategies++;
      fprintf(stderr, "Strategy path found (%d) : %s\n", nb_strategies, argv[i]);
    }
    else {
      fprintf(stderr, "Strategy path not found : %s\n", argv[i]);
      exit(EXIT_FAILURE);
    }
  }

  gp.strategies_paths = (char **) strategies;
  gp.nb_strategies = nb_strategies;

  fprintf(stderr, "nb strategies : %d\n", gp.nb_strategies);

  return game_exec(gp);
}
