

#ifndef  __WORLD_CONF_H__
#define  __WORLD_CONF_H__

/* World limits */

#define MAX_PLAYERS 10

#define MAX_PENGUINS_PER_PLAYER 10

#define MAX_TILES 100

#define MAX_FISH_PER_TILE 3

#endif
