
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "interface_strategy_server.h"
#include "interface_strategy_client.h"



int sum_array (int *a, int n) {
  int sum = 0;
  for (int i = 0; i < n; i++) {
    sum = sum + a[i];
  }
  return sum;
}


int place_peng() {
  unsigned int b = api_server__nb_starting_tiles();
  unsigned int a = rand() % b;
  unsigned int tile = api_server__get_starting_tile(a);

  api_server__place_penguin (tile);

  return EXIT_SUCCESS;
}


int move_peng(unsigned int peng, unsigned int current_player) {
  unsigned int peng_tile = api_server__get_penguin_tile(current_player, peng);
  unsigned int nb_dir = api_server__nb_directions(peng_tile);

  if (nb_dir == 0) {
    return EXIT_FAILURE;
  }

  int *blocked_directions = malloc(sizeof(int) * nb_dir);
  bzero(blocked_directions, nb_dir * sizeof(int));

  unsigned int dir = rand() % nb_dir;
  int t = api_server__get_tile(peng_tile, dir, 1);
  while(api_server__is_free(t) != 1 && sum_array(blocked_directions, nb_dir) < nb_dir) {
    blocked_directions[dir] = 1;
    dir = rand() % nb_dir;
    t = api_server__get_tile(peng_tile, dir, 1);
  }

  if (sum_array(blocked_directions, nb_dir) < nb_dir) {
    //fprintf(stderr, "t=%d, dir=%u : is free\n", t, dir);
    api_server__move_penguin(peng, dir, 1);
    free(blocked_directions);
    return EXIT_SUCCESS;
  }
  else {
    //fprintf(stderr, "t=%d, dir=%u : not free (next penguin)\n", t, dir);
    free(blocked_directions);
    return EXIT_FAILURE;
  }
}


int api_client__setup () {
  return 0;
}


int api_client__cleanup () {
  return 0;
}



int api_client__run () {
  unsigned int current_player = api_server__current_player ();
  int nb_peng = api_server__nb_penguins (current_player);
  int nb_peng_placed = api_server__nb_penguins_placed (current_player);

  if((nb_peng - nb_peng_placed) > 0) {
    place_peng();
  }
  else {
    int ret_move = EXIT_FAILURE;
    int *bloked_penguins = malloc(sizeof(int) * nb_peng);
    assert(bloked_penguins != NULL);
    bzero(bloked_penguins, nb_peng * sizeof(int));
    while(sum_array(bloked_penguins, nb_peng) < nb_peng && (ret_move == EXIT_FAILURE)) {
      unsigned int peng = rand() % nb_peng;
      bloked_penguins[peng] = 1;
      ret_move = move_peng(peng, current_player);
    }
    free(bloked_penguins);
  }

  return EXIT_SUCCESS;
}
