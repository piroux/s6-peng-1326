
#include "player.h"


/* Initialisateur de joueur */
void player__init (struct player *pl, int nb_penguins_per_player, char *name) {
  static int nb_players = 0;
  pl->id = nb_players++;

  pl->name = name;

  assert(nb_penguins_per_player <= MAX_PENGUINS_PER_PLAYER);
  pl->nb_penguins_max = nb_penguins_per_player;

  for (int i = 0; i < pl->nb_penguins_max; i++) {
    penguin__init (&pl->penguins[i], pl->id, i);
  }

  pl->nb_penguins_placed = 0;

  pl->score_tiles = 0;
  pl->score_fishes = 0;
  pl->score_distance = 0;
}


/* Méthode de copie de joueurs */
void player__copy (struct player *pl_dst, struct player *pl_src) {
  pl_dst->id = pl_src->id;
  pl_dst->name = pl_src->name;
  pl_dst->nb_penguins_max = pl_src->nb_penguins_max;
  pl_dst->nb_penguins_placed = pl_src->nb_penguins_placed;

  for (int i = 0; i < pl_dst->nb_penguins_max; i++) {
    penguin__copy (&pl_dst->penguins[i], &pl_src->penguins[i]);
  }

  pl_dst->score_fishes = pl_src->score_fishes;
  pl_dst->score_tiles = pl_src->score_tiles;
  pl_dst->score_distance = pl_src->score_distance;
}


/* Déstructeur de joueur */
void player__destroy (struct player *pl) {
  for (int i = 0; i < pl->nb_penguins_max; i++) {
    penguin__destroy (&pl->penguins[i]);
  }
}


/* Actualisateur du score du joueur */
void player__score (struct player *pl, unsigned int fishes, unsigned int distance) {
  pl->score_tiles++;
  pl->score_fishes = pl->score_fishes + fishes;
  pl->score_distance = pl->score_distance + distance;
}

/* Comparateur de score de joueurs (utilisé par qsort) */
int player__cmp_rank (const void *pl1_, const void *pl2_) {
  struct player *pl1 = *(struct player **) pl1_;
  struct player *pl2 = *(struct player **) pl2_;

  int rank;
  /* qsort sort in ascending order. */

  if (pl2->score_fishes != pl1->score_fishes) {
    rank = pl2->score_fishes - pl1->score_fishes;
  }
  else if (pl2->score_tiles != pl1->score_tiles) {
    rank = pl2->score_tiles - pl1->score_tiles;
  }
  else {
    rank = pl2->score_distance - pl1->score_distance;
  }

  return rank;
}


/* Vérificateur de fin de partie pour le joueur

  Renvoie
    1 si le joueur courant peut continuer à jouer,
    0 sinon
*/
int player__is_finished (struct player *pl) {
  /* Pour chaque pingouin du joueur : */
  for (int pg_id = 0; pg_id < pl->nb_penguins_max; pg_id++) {
    /* Si il y en a au moins un de non bloqué, le joueur n'a pas fini. */
    if (!penguin__is_finished(&pl->penguins[pg_id])) {
      return 0;
    }
  }

  return 1;
}
