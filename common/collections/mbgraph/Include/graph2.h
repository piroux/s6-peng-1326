/*
 *  graph2.h - a graph as an adjacency list
 *  Copyright (C) 2010 Martin Broadhurst
 *  www.martinbroadhurst.com
 */

#ifndef GRAPH2_H
#define GRAPH2_H

#include <vertex.h>
#include <edge.h>
#include <set.h>
#include <iterator.h>

typedef struct {
	MBset *vertices;
} MBgraph2;

MBgraph2 *   MBgraph2_create(void);
void         MBgraph2_delete(MBgraph2 *graph);
MBvertex *   MBgraph2_add(MBgraph2 *graph, const char *name, void *data);
MBvertex *   MBgraph2_get_vertex(const MBgraph2 *graph, const char *name);
void *       MBgraph2_remove(MBgraph2 *graph, MBvertex *vertex);
void         MBgraph2_add_edge(MBgraph2 *graph, MBvertex *vertex1, MBvertex *vertex2);
void         MBgraph2_remove_edge(MBgraph2 *graph, MBvertex *vertex1, MBvertex *vertex2);
unsigned int MBgraph2_get_adjacent(const MBgraph2 *graph, const MBvertex *vertex1, const MBvertex *vertex2);
MBiterator * MBgraph2_get_neighbours(const MBgraph2 *graph, const MBvertex *vertex);
MBiterator * MBgraph2_get_edges(const MBgraph2 *graph);
MBiterator * MBgraph2_get_vertices(const MBgraph2 *graph);
unsigned int MBgraph2_get_neighbour_count(const MBgraph2 * graph, const MBvertex * vertex);
unsigned int MBgraph2_get_edge_count(const MBgraph2 * graph);
unsigned int MBgraph2_get_vertex_count(const MBgraph2 * graph);

#endif /* GRAPH2_H */
