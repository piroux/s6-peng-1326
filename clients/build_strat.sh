#!/bin/bash

WORKDIR=$(pwd)

if [[ ${#} != 1 ]]
then
	echo "Usage $0: <strategy name>"
	exit 1
fi

strat=${1}

if [[ ! -d ${strat}  ]]
then
	echo "path not found : ${strat}"
	exit 1
fi

set -x

cd ${strat} && rm -rf build && mkdir build && cd build && cmake .. && make && ls -l
