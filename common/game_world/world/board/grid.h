
#ifndef  __GRID_H__
#define  __GRID_H__

#include <assert.h>
#include <string.h>

#include "graph2.h"
#include "vertex.h"
#include "edge.h"

#include "log.h"
#include "tessel.h"
#include "tile.h"
#include "../world_conf.h"


struct grid {
  struct tile tiles [MAX_TILES];
  unsigned int nb_tiles;

  struct tessel tessel_grid;

  MBgraph2 *graph;
};


void grid__init (struct grid *gd, unsigned int nb_tiles, enum tessel_kind tk);
void grid__copy (struct grid *gd_dst, struct grid *gd_src);
void grid__destroy (struct grid *gd);

void grid__print(struct grid *gd);

unsigned int grid__nb_starting_tiles (struct grid *gd);
unsigned int grid__get_starting_tile (struct grid *gd, unsigned int starting_tile_id);

unsigned int grid__nb_directions (struct grid *gd, unsigned int tile_id);
unsigned int grid__get_global_dir (struct grid *gd, unsigned int tile_id, unsigned int direction);
int grid__validate_path (struct grid *gd, unsigned int starting_tile_id, unsigned int direction, unsigned int distance);

void grid__takeoff_tile (struct grid *gd, unsigned int tile_id);
void grid__landing_tile (struct grid *gd, unsigned int tile_id, unsigned int penguin_id);

int grid__has_free_neighbours (struct grid *gd, unsigned int tile_id);
unsigned int grid__get_first_free_neighbour (struct grid *gd, unsigned int tile_id);
unsigned int grid__nb_free_neighbours (struct grid *gd, unsigned int tile_id);


#endif
