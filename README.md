# Project Penguin

This project is an implementation of the board game [Hey, that’s my fish"](https://www.google.com/search?q=Hey,+that%E2%80%99s+my+fish)
which offers each player to create his own strategy. Each strategy is a shared-object library which has to implement a API understood by the game server.
The game server is responsible for checking the actions of all running strategies and eventually to apply them to its model of the board.

**Side Note :** The output of the game server is only textual.

## Layout

```
.
|-- clients               : Client strategies from our team and from other teams.
|   |-- build_strat.sh
|   |-- other_teams
|   |-- strategy_empty
|   |-- strategy_greedy
|   `-- strategy_random
|-- common                : Common code which can be used by clients and the server at the same time.
|   |-- collections
|   |-- game_world
|   |-- interfaces
|   `-- utils
|-- README
`-- server                : Host server for the game "Hey that's my fish".
    |-- build.sh
    `-- src
```

## Requirements

The following tools are needed to compile and test the different components of the project :

  - gcc
  - make
  - cmake


## Launch Instructions


### Client Strategies

To start using the server, you need at least 2 clients.

To compile one strategy:
```
$ cd clients
$ ./build_strat.sh <strategy name>
...
```

The resulting shared object for the selected strategy will be available at :

  - `clients/<strategy name>/build/lib<strategy name>1.so`
  - `clients/<strategy name>/build/lib<strategy name>2.so`


### Game Server

The server allows you to play a match between strategies.

To compile the server, from the root of the project:
```
$ cd server
$ ./build.sh
...
```

The resulting executable will be available at : `server/build/htmf`

Finally, you can start using the server by providing at least two client paths to the executable.
For instance, from the root of the project:

```
$ ./server/build/htmf clients/strategy_random/build/librandom1.so clients/strategy_random/build/librandom2.so
```

## Tests Instructions


There are only tests for the internals game data-structures in `common/game_world`

To compile and tests this component, from the root of the project:

```
$ cd common/game_world
$ ./build.sh
...
```


## Notes


### Log output

If you want no log output : you have to set the flag USE_LOG to 0 in :
`common/utils/log.h`

If you want to setup the log output, you have to set the flag LOG_LEVEL in :
`common/utils/log.h`

In the log output, you may see the following textual artifact :
It represents a tile board.

```x:y[i]{s}```

where :

  - `x:y` is the offset coordinate of the tile in the graph
  - `i` is the id of the tile
  - `s` is the tile state: `0=FREE`, `1=TAKEN`, `2=DEAD`

Moreover, the graph of the board may be printed as an Incidence List where each vertex/tile is represented
by the following textual representation :

```* 0:0[ 0]{0} (2)-> 0:1[ 8]1{0} , 1:0[ 1]0{0}```

Here, one more information is provided : the global direction from the source vertex to each of his neighbours
as `d` in `x:y[i]d{s}`.


### Debugging

An easy way to launch the game in gdb :

```
$ cd server
$ gdb -ex run --args ./build/htmf ../clients/strategy_random/build/librandom1.so ../clients/strategy_random/build/librandom2.so
```


## License


The project is licensed under the MIT license.