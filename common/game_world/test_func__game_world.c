
#include <stdlib.h>
#include <stdio.h>

#include "game_world.h"


void log_test(char *msg) {
  printf("%s", msg);
}


int main(int argc, char  **argv) {

  struct game_world gw;
  struct game_action ga;
  // void game_world__init(struct game_world *gw, char **player_names, int nb_players, int nb_tiles, enum tessel_kind tk);

  #define _nb_players 2
  #define _nb_penguins_per_player 3
  char *player_names[_nb_players];
  player_names[0] = "player1";
  player_names[1] = "player2";

  game_world__init(&gw, player_names, _nb_players, _nb_penguins_per_player, 60, TESSEL_HEXA);
  game_action__init(&ga);
  game_world__print(&gw);


  game_action__reset(&ga);
  game_action__do_place(&ga, 0, 2);
  assert(game_world__check_action(&gw, &ga, log_test));
  game_world__apply_action(&gw, &ga);
  game_world__print(&gw);
  game_world__prepare_next_round(&gw);

  game_action__reset(&ga);
  game_action__do_place(&ga, 1, 3);
  assert(game_world__check_action(&gw, &ga, log_test));
  game_world__apply_action(&gw, &ga);
  game_world__print(&gw);
  game_world__prepare_next_round(&gw);

  game_action__reset(&ga);
  game_action__do_place(&ga, 0, 4);
  assert(game_world__check_action(&gw, &ga, log_test));
  game_world__apply_action(&gw, &ga);
  game_world__print(&gw);
  game_world__prepare_next_round(&gw);

  game_action__reset(&ga);
  game_action__do_place(&ga, 1, 5);
  assert(game_world__check_action(&gw, &ga, log_test));
  game_world__apply_action(&gw, &ga);
  game_world__print(&gw);
  game_world__prepare_next_round(&gw);

  game_action__reset(&ga);
  game_action__do_place(&ga, 0, 6);
  assert(game_world__check_action(&gw, &ga, log_test));
  game_world__apply_action(&gw, &ga);
  game_world__print(&gw);
  game_world__prepare_next_round(&gw);

  game_action__reset(&ga);
  game_action__do_place(&ga, 1, 7);
  assert(game_world__check_action(&gw, &ga, log_test));
  game_world__apply_action(&gw, &ga);
  game_world__print(&gw);
  game_world__prepare_next_round(&gw);


  game_action__reset(&ga);
  game_action__do_move(&ga, 0, 0, 1, 3);
  assert(game_world__check_action(&gw, &ga, log_test));
  game_world__apply_action(&gw, &ga);
  game_world__print(&gw);
  game_world__prepare_next_round(&gw);




  printf("Copie :\n");
  struct game_world gw2;
  game_world__copy(&gw2, &gw);
  game_world__print(&gw);


  return EXIT_SUCCESS;
}
