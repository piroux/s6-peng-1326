
#ifndef _UTILS_RANDOM_H_
#define _UTILS_RANDOM_H_

#include "stdlib.h"
#include "math.h"
#include "time.h"


void seed_random(unsigned value);

int random_integer(int max);
float random_float(float max);
int random_boolean();


#endif
