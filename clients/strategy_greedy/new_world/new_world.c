
#include "new_world.h"


//construct a world from interface function
struct world* world__construct(){
    unsigned int nb_players = api_server__nb_players ();
    unsigned int nb_peng = api_server__nb_penguins(0);
    
    struct world* w = malloc(sizeof(struct world));
    w->nb_players = nb_players;
    w->nb_penguins_per_player = nb_peng;
    graph__construct(&(w->g));

    for(int i=0;i<nb_players;i++)
        player__construct(&w->g,&w->players[i],i,nb_peng);

    return w;
}


void world_free(struct world* w){
  struct tile* tmp = NULL;
    
    for (int i = 0; i < w->g.nb_tiles; i++) {
        tmp = w->g.tiles;
        free(w->g.tiles);
        w->g.tiles = tmp++;
    }

    free(w);
}
