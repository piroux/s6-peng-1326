#include "parcours.h"

//recursive function that traverses the graph from the tile of the penguin
int alone_rec (struct world* w, int id_tile, int* colours){

    //if colours[id_tile] = 1 a penguin of another player is here
    if (colours[id_tile] != 0)
        return colours[id_tile] - 1;
    
    colours[id_tile] = 1;
        
    if (w->g.tiles[id_tile].id_player != -1 && w->g.tiles[id_tile].id_player != api_server__current_player ())
        return 0;
    
    colours[id_tile] = 2;
    
    for (int i = 0; i<MAX_ADJ; i++) {

        if (w->g.tiles[id_tile].adj[i] != -1) 
            alone_rec(w,w->g.tiles[id_tile].adj[i], colours);
    }
    return 1;
}



// Find if a player is alone on an island and return a table colours of size the number of tile in the graph, where if colours[id_tile] = 2, the tile belongs to the island; if the penguin is not alone return NULL  
int* alone (struct world* w, int id_tile){

    int * colours = calloc(w->g.nb_tiles,sizeof(int));
    
    if( alone_rec(w, id_tile, colours) == 0){
        free(colours);
        colours = NULL;
    }
    return colours;
}


int parcours_rec (struct world* w, int* number, int* pere, int id_tile,int depth){
    pere[depth - 1] = id_tile;
    
    for (int i = 0; i<MAX_ADJ; i++) {
        int is_pere = 0;
        if (w->g.tiles[id_tile].adj[i] != -1) {
            for (int j = 0; pere[j] > 0; j++){
                if (pere[j] == w->g.tiles[id_tile].adj[i]) {
                    is_pere = 1;
                }
            }
            if (is_pere == 0){
                if (number[w->g.tiles[id_tile].adj[i]] != 0 && number[w->g.tiles[id_tile].adj[i]] < depth) {
                    number[w->g.tiles[id_tile].adj[i]] = depth;

                }
                number[w->g.tiles[id_tile].adj[i]] = depth;
                parcours_rec(w,number,pere,w->g.tiles[id_tile].adj[i],depth+1);
            }
        }
    }
    return 1;
}

int* parcours(struct world* w, int* colours, int id_tile){
    int nb = 0;
    
    for (int i = 0; i<w->g.nb_tiles; i++) {
        if (colours[i] == 2)
            nb++;
    }
    
    int * pere = calloc(nb,sizeof(int));
    int * number = calloc(w->g.nb_tiles,sizeof(int));
    
    parcours_rec(w,number,pere,id_tile,1);
    
    free(number);
    return pere;
}






