
#ifndef  __TILE_H__
#define  __TILE_H__

#include <assert.h>

#include "tcoord.h"
#include "tessel.h"
#include "../world_conf.h"


enum tile_state {
  FREE,
  TAKEN,
  DEAD
};


struct tile {
  unsigned int id;

  /* */
  struct tcoord coord;

  enum tile_state state;

  /* Nombre de poisson(s) sur la tuile */
  unsigned int nb_fish;


  int penguin_id;
};


void tile__init (struct tile *ti, struct tessel *tg);
void tile__copy (struct tile *ti_dst, struct tile *ti_src);
void tile__destroy (struct tile *ti);


#endif
