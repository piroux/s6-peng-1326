

cmake_minimum_required(VERSION 2.8)
project(penguins-game_world)


## Utils

include_directories(
  ../utils
)

set (utils_SRC
  ../utils/log.c
  ../utils/num.c
  ../utils/random.c
)


## Game World

include_directories(
  ../collections/mbgraph/Include
)

set (graph_SRC
  ../collections/mbgraph/Src/dynarray.c
  ../collections/mbgraph/Src/edge.c
  ../collections/mbgraph/Src/graph2.c
  ../collections/mbgraph/Src/iterator.c
  ../collections/mbgraph/Src/set.c
  ../collections/mbgraph/Src/vertex.c
)

set(test_func__game_world_SRC
  ${graph_SRC}
  ${utils_SRC}
  world/board/grid.c
  world/board/tcoord.c
  world/board/tessel.c
  world/board/tile.c
  world/penguin.c
  world/player.c
  world/world.c
  game_action.c
  game_world.c
  test_func__game_world.c
)

set(test_unit__game_world_SRC
  ${graph_SRC}
  ${utils_SRC}
  world/board/grid.c
  world/board/tcoord.c
  world/board/tessel.c
  world/board/tile.c
  world/penguin.c
  world/player.c
  world/world.c
  game_action.c
  game_world.c
  test_unit__game_world.c
)

#add_definitions(-ggdb -std=c99 -Wall -Werror -pedantic)
add_definitions(-ggdb -std=gnu99 -Wall -pedantic)


add_executable(test_unit__game_world ${test_unit__game_world_SRC})
target_link_libraries(test_unit__game_world m)

add_executable(test_func__game_world ${test_func__game_world_SRC})
target_link_libraries(test_func__game_world m)


enable_testing()
add_test(unit__game_world test_unit__game_world)
add_test(func__game_world test_func__game_world)
