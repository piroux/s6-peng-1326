cmake_minimum_required(VERSION 2.8)
project(strategy-empty)


include_directories(
  ../../common/interfaces/
  ../../common/game_world/
)


set(strategy_SRC
  #utils/log.c
  #utils/num.c
  #utils/random.c
  #../../common/game_world/game_world.c
  empty.c)

message("Recorded *.c :" ${strategy_SRC})

add_definitions(-ggdb -std=gnu99 -Wall -Werror -pedantic)


add_library(empty1 SHARED ${strategy_SRC})
add_library(empty2 SHARED ${strategy_SRC})

#target_link_libraries(htmf dl m)
