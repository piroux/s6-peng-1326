cmake_minimum_required(VERSION 2.8)
project(strategy-greedy)


include_directories(
  ../../common/interfaces/
  ../../common/game_world/
  new_world/
)


set(strategy_SRC
  #utils/log.c
  #utils/num.c
  #utils/random.c
  #../../common/game_world/game_world.c
  new_world/new_player.c
  new_world/new_world.c
  new_world/new_penguin.c
  new_world/graph.c
  new_world/parcours.c
  greedy.c)

message("Recorded *.c :" ${strategy_SRC})

add_definitions(-ggdb -std=gnu99 -Wall -pedantic)


add_library(greedy1 SHARED ${strategy_SRC})
add_library(greedy2 SHARED ${strategy_SRC})

#target_link_libraries(htmf dl m)
