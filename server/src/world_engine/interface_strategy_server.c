

#include "interface_strategy_server.h"

#include "world_engine.h"
#include "game_world.h"
#include "graph2.h"

#include "log.h"


struct game_world *gw;
struct game_action *ga;


/* Implémentation de l'interface de programmation (API) pour le modèle de jeu
développé (game_world).

  Les fonctions d'interface acceptant des paramètres doivent vérifier que
  ceux-ci soient corrects.

  Il a été décidé de fournir une interface très stricte, de telle sorte que si
  une stratégie fait un appel de mouvement (place/move) incorrect, elle aura
  directement perdu. Cela se traduit, avec des paramètres incorrectes, par la
  non application du mouvement demandé au modèle de jeu de l'interface.

  Les autres fonctions de l'interface tolèrent des paramètres d'entrée
  incorrects, cad qu'elles ne font pas perdre la stratégie. Mais dans ce cas là,
  elles ne modifient pas non plus le modèle de jeu associé, et elles peuvent
  renvoyer volontairement une valeur incohérente (0xdead....).

*/


void game_world_server__attach(struct game_world *_gw, struct game_action *_ga) {
  gw = _gw;
  ga = _ga;
}


unsigned int api_server__nb_players () {
  log_trace("", NULL);
  log_trace("=> nb_players=%u", gw->w.nb_players);
  return gw->w.nb_players;
}


unsigned int api_server__current_player () {
  log_trace("", NULL);
  return gw->current_player_id;
}


unsigned int api_server__get_penguin_tile (unsigned int player_id, unsigned int penguin_id) {
  log_trace("player_id=%u, penguin_id=%u", player_id, penguin_id);

  if (!(player_id < gw->w.nb_players)) {
    log_warning("=> strategy input error: player_id=%u >= nb_players=%u", player_id, gw->w.nb_players);
    return 0xdeadfa11;
  }

  if (!(penguin_id < gw->w.players[player_id].nb_penguins_placed)) {
    log_warning("=> strategy input error: penguin_id=%u >= nb_penguins_placed=%u", penguin_id, gw->w.players[player_id].nb_penguins_placed);
    return 0xdeadfa11;
  }

  unsigned int penguin_tile_id = gw->w.players[player_id].penguins[penguin_id].tile_id;

  log_trace("=> penguin_tile_id=%u", penguin_tile_id);
  return penguin_tile_id;
}


int api_server__nb_fish (unsigned int tile_id) {
  log_trace("tile_id=%u", tile_id);

  if(!(tile_id < gw->w.grid_board.nb_tiles)) {
    log_warning("=> strategy input error: tile_id=%u >= nb_tiles=%u", tile_id, gw->w.grid_board.nb_tiles);
    return 0xdeadfa11;
  }

  return gw->w.grid_board.tiles[tile_id].nb_fish;
}


unsigned int api_server__nb_directions (unsigned int tile_id) {
  log_trace("tile_id=%u", tile_id);

  if(!(tile_id < gw->w.grid_board.nb_tiles)) {
    log_warning("=> strategy input error: tile_id=%u >= nb_tiles=%u", tile_id, gw->w.grid_board.nb_tiles);
    return 0xdeadfa11;
  }

  unsigned int nb_directions = grid__nb_directions(&gw->w.grid_board, tile_id);

  log_trace("=> nb_directions=%u", nb_directions);

  return nb_directions;
}


unsigned int api_server__get_tile (unsigned int tile_id, unsigned int direction, unsigned int distance) {
  log_trace("tile_id=%u, direction=%u, distance=%u", tile_id, direction, distance);

  if(!(tile_id < gw->w.grid_board.nb_tiles)) {
    log_warning("=> strategy input error: tile_id=%u >= nb_tiles=%u", tile_id, gw->w.grid_board.nb_tiles);
    return 0xdeadfa11;
  }

  /* warning : here direction is an available direction
    while grid__validate_path takes a global direction. */

  unsigned int global_dir = grid__get_global_dir(&gw->w.grid_board, tile_id, direction);

  int valid_path = grid__validate_path(&gw->w.grid_board, tile_id, global_dir, distance);
  log_trace("=> valid_path=%d", valid_path);

  if (valid_path < 0) {
    valid_path = 0xdeadbeef;
  }

  return valid_path;
}


int api_server__is_free (unsigned int tile_id) {
  log_trace("tile_id=%u", tile_id);

  int is_free;

  if (tile_id == 0xdeadbeef || tile_id == 0xdeadfa11) {
    is_free = 0;
    log_trace("=> is_free=%d (discarded)", is_free);
  }
  else if (!(tile_id < gw->w.grid_board.nb_tiles)) {
    is_free = 0;
    log_trace("=> is_free=%d (too high)", is_free);
  }
  else {
    is_free = (gw->w.grid_board.tiles[tile_id].state == FREE);
    log_trace("=> is_free=%d (state)", is_free);
  }

  return is_free;
}


int api_server__place_penguin (unsigned int tile_id) {
  log_trace("tile_id=%u", tile_id);

  if(!(tile_id < gw->w.grid_board.nb_tiles)) {
    log_warning("=> strategy input error: tile_id=%u >= nb_tiles=%u", tile_id, gw->w.grid_board.nb_tiles);
    return 0xdeadfa11;
  }

  game_action__do_place(ga, gw->current_player_id, tile_id);

  int action_valid = game_world__check_action(gw, ga, NULL);

  if (action_valid > 0) {
    game_world__apply_action(gw, ga);
    log_trace("=> action valid: applied (%d)", action_valid);
    return 1;
  }
  else {
    log_warning("=> invalid action: not applied (%d)", action_valid);
    return 0;
  }
}


int api_server__move_penguin (unsigned int penguin_id, unsigned int direction, unsigned int distance) {
  log_trace("penguin_id=%u, direction=%u, distance=%u", penguin_id, direction, distance);

  unsigned int player_id = gw->current_player_id;

  if (!(penguin_id < gw->w.players[player_id].nb_penguins_placed)) {
    log_warning("=> strategy input error: penguin_id=%u >= nb_penguins_placed=%u", penguin_id, gw->w.players[player_id].nb_penguins_placed);
    return 0xdeadfa11;
  }

  unsigned int nb_directions = grid__nb_directions(&gw->w.grid_board, gw->w.players[player_id].penguins[penguin_id].tile_id);
  if (!(direction < nb_directions)) {
    log_warning("=> strategy input error: direction=%u >= nb_directions=%u", direction, nb_directions);
    return 0xdeadfa11;
  }

  /* warning : here direction is an available direction
    while game_action__do_move takes a global direction. */

  unsigned int global_dir = grid__get_global_dir(&gw->w.grid_board, gw->w.players[player_id].penguins[penguin_id].tile_id, direction);

  game_action__do_move(ga, player_id, penguin_id, global_dir, distance);

  int action_valid = game_world__check_action(gw, ga, NULL);

  if (action_valid > 0) {
    game_world__apply_action(gw, ga);
    log_trace("=> action valid: applied (%d)", action_valid);
    return 1;
  }
  else {
    log_warning("=> invalid action: not applied (%d)", action_valid);
    return 0;
  }
}


unsigned int api_server__direction_type (unsigned int tile_id, unsigned int direction) {
  log_trace("tile_id=%u, direction=%u", tile_id, direction);

  if(!(tile_id < gw->w.grid_board.nb_tiles)) {
    log_warning("=> strategy input error: tile_id=%u >= nb_tiles=%u", tile_id, gw->w.grid_board.nb_tiles);
    return 0xdeadfa11;
  };

  unsigned int nb_directions = grid__nb_directions(&gw->w.grid_board, tile_id);

  if(!(direction < nb_directions)) {
    log_warning("=> strategy input error: direction=%u >= nb_directions=%u", direction, nb_directions);
    return 0xdeadfa11;
  }

  return grid__get_global_dir(&gw->w.grid_board, tile_id, direction);
}


int api_server__player_score_fish (unsigned int player_id) {
  log_trace("player_id=%u", player_id);

  if (!(player_id < gw->w.nb_players)) {
    log_warning("=> strategy input error: player_id=%u >= nb_players=%u", player_id, gw->w.nb_players);
    return 0xdeadfa11;
  }

  return gw->w.players[player_id].score_fishes;
}


int api_server__player_score_tiles (unsigned int player_id) {
  log_trace("player_id=%u", player_id);

  if (!(player_id < gw->w.nb_players)) {
    log_warning("=> strategy input error: player_id=%u >= nb_players=%u", player_id, gw->w.nb_players);
    return 0xdeadfa11;
  }

  return gw->w.players[player_id].score_tiles;
}


unsigned int api_server__nb_penguins (unsigned int player_id) {
  log_trace("player_id=%u", player_id);

  if (!(player_id < gw->w.nb_players)) {
    log_warning("=> strategy input error: player_id=%u >= nb_players=%u", player_id, gw->w.nb_players);
    return 0xdeadfa11;
  }

  unsigned int nb_penguins_max = gw->w.players[player_id].nb_penguins_max;

  log_trace("=> nb_penguins_max=%u", nb_penguins_max);
  return nb_penguins_max;
}


unsigned int api_server__nb_penguins_placed (unsigned int player_id) {
  log_trace("player_id=%u", player_id);

  if (!(player_id < gw->w.nb_players)) {
    log_warning("=> strategy input error: player_id=%u >= nb_players=%u", player_id, gw->w.nb_players);
    return 0xdeadfa11;
  }

  unsigned int nb_penguins_placed = gw->w.players[player_id].nb_penguins_placed;

  log_trace("=> nb_penguins_placed=%u", nb_penguins_placed);
  return nb_penguins_placed;
}


unsigned int api_server__nb_starting_tiles () {
  log_trace("", NULL);

  unsigned int nb_starting_tiles = grid__nb_starting_tiles(&gw->w.grid_board);

  log_trace("=> nb_starting_tiles=%u", nb_starting_tiles);
  return nb_starting_tiles;
}


unsigned int api_server__get_starting_tile (unsigned int starting_tile_id) {
  log_trace("starting_tile_id=%u", starting_tile_id);

  unsigned int nb_starting_tiles = grid__nb_starting_tiles(&gw->w.grid_board);

  if(!(starting_tile_id < nb_starting_tiles)) {
    log_warning("=> strategy input error: starting_tile_id=%u >= nb_starting_tiles=%u", starting_tile_id, nb_starting_tiles);
    return 0xdeadfa11;
  }

  unsigned int tile_id = grid__get_starting_tile(&gw->w.grid_board, starting_tile_id);

  log_trace("=> tile_id=%u", tile_id);
  return tile_id;
}


int api_server__version () {
  log_trace("", NULL);

  return __INTERFACE_STRATEGY_SERVER_VERSION__;
}
