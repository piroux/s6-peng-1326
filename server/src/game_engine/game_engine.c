
#include "game_engine.h"


/* Game Engine : Moteur principal du serveur de jeu.

  Ce composant est responsable du l'ensemble du jeu, et se trouve tout en haut
  de la hierarchie d'exécution du serveur.
  Il est l'interface de haut-niveau faisant évoluer le modèle du jeu au fur et
  à mesure que la partie se déroule.

  Il contient donc à la fois le gestionnaire de stratégie
  (strategy_engine) et le gestionnaire de modèle de jeu (game_world) : il doit
  donc gérer leur construction et leur destruction. De plus il est le seul
  composant à avoir accès à l'un et à l'autre. En effet, le world_engine n'a
  pas connaissance du strategy_engine, et vice versa.
  Enfin il possède la boucle de jeu faisant fonctionner le serveur :

    * construction du jeu
    * tant que le jeu n'est pas fini
      - exécution de tour suivant
    * affichage du classement final
    * destruction du jeu

*/


/* Constructeur pour le game_engine */
void game_engine__setup(struct game_engine *gm_eng) {
  log_debug("started", NULL);

  gm_eng->gp.winner = -1;
  gm_eng->gp.error = 0;

  /* Construction du world_engine */
  world_engine__setup(&gm_eng->wd_eng, &gm_eng->gp);

  /* Construction du strategy_engine */
  strategy_engine__setup(&gm_eng->st_eng, &gm_eng->gp);

  log_debug("finished", NULL);
}


/* Destructeur pour le game_engine */
void game_engine__cleanup(struct game_engine *gm_eng) {
  log_debug("started", NULL);

  /* Construction du world_engine */
  strategy_engine__cleanup(&gm_eng->st_eng);

  /* Destruction du strategy_engine */
  world_engine__cleanup(&gm_eng->wd_eng);

  log_debug("finished", NULL);
}


/* Vérificateur de fin de partie

  Renvoie
    1 si le modèle ne doit pas continuer à évoluer,
    0 sinon
*/
int game_engine__is_finished(struct game_engine *gm_eng) {
  return world_engine__is_finished(&gm_eng->wd_eng);
}


/* Itérateur du jeu pour le tour en cours */
void game_engine__next(struct game_engine *gm_eng) {
  log_debug("started : Starting new turn [%3u] --------------------------------v", gm_eng->wd_eng.world_backend.current_round + 1);
  log_debug("current strategy : %s", gm_eng->gp.strategies_paths[gm_eng->gp.current_strategy]);

  /* Préparation du modèle de jeu pour le nouveau tour */
  world_engine__next(&gm_eng->wd_eng);

  /* Si la stratégie peut jouer, le flux d'exécution
  est passé au strategy_engine. */
  if (world_engine__can_play(&gm_eng->wd_eng)) {
    strategy_engine__next(&gm_eng->st_eng);
    log_debug("strategy played", NULL);
  }
  /* Sinon elle passe son tour. */
  else {
    log_debug("cannot play: pass", NULL);
  }

  /* Affichage de l'action enregistrée durant le dernier tour d'éxécution de
  la stratégie */
  log_raw_if(DEBUG, game_action__print(&gm_eng->wd_eng.action));

  /* Application de l'action demandée par la stratégie durant
  son tour au modèle de jeu original. */
  world_engine__check(&gm_eng->wd_eng);
  world_engine__apply_action(&gm_eng->wd_eng);
  world_engine__check(&gm_eng->wd_eng);

  // UI
  /* Affichage de l'état du modèle du jeu */
  log_debug("round final state: ", NULL);
  log_raw_if(DEBUG, game_world__print(&gm_eng->wd_eng.world_backend));

  /* Itération de l'identifiant de stratégie
    Cette itération doit être réalisée pour s'assurer que le game_engine
    et le world_engine restent synchrones avec l'itération parallèle du
    joueur dans le modèle de jeu (game_world). */
  gm_eng->gp.current_strategy = (gm_eng->gp.current_strategy + 1) % gm_eng->gp.nb_strategies;

  /* Préparateur du tour suivant et vérification de joueurs bloqués */
  world_engine__prepare_next(&gm_eng->wd_eng);

  /* Vérification de déclenchement d'erreur potentielle dûe à la dernière
  stratégie */
  gm_eng->gp.error = world_engine__check_error(&gm_eng->wd_eng);

  log_debug("finished ---------------------------------------------------------^", NULL);
}


/* Afficheur du classement de fin de partie */
void game_engine__report(struct game_engine *gm_eng) {
  gm_eng->gp.winner = world_engine__report_rankings(&gm_eng->wd_eng);
}


/* Boucle de jeu principale

  Tout le flux d'éxécution du serveur est contenu dans cette fonction.
  L'ensemble des méthodes du game_world sont appelées ici, qui elles-même
  font appel aux méthodes du world_engine et du strategy_engine.
*/
int game_exec(struct game_parameters gp) {

  struct game_engine gm_eng;

  gm_eng.gp = gp;

  assert( (TESSEL_SHAPE == TESSEL_HEXA || TESSEL_SHAPE == TESSEL_SQUARE)
    && "This kind of tesselation is not implemented");

  assert( (NB_PENGUINS_PER_PLAYER <= MAX_PENGUINS_PER_PLAYER)
    && "The chosen number of penguins per player is too high.");

  assert( (NB_TILES < MAX_TILES)
    && "The chosen number of tiles for the board is too high.");

  if (!(gp.nb_strategies >= 2)) {
    log_error("The server needs at least two strategies to start.", NULL);
    return -1;
  }
  if (gp.nb_strategies > MAX_PLAYERS) {
    log_error("The server is not configured to handled this amount of strategies = %d > max=%d", gp.nb_strategies, MAX_PLAYERS);
    return -1;
  }
  if ((gp.nb_strategies * NB_PENGUINS_PER_PLAYER) >= NB_TILES) {
    log_error("The server will not be able to place all penguins.", NULL);
    return -1;
  }

  /* Création du game_engine */
  game_engine__setup(&gm_eng);

  /* Si la partie peut continuer ... */
  while (!game_engine__is_finished(&gm_eng)) {
    /* ... le moteur de jeu itère un tour de plus, */
    game_engine__next(&gm_eng);
  }
  /* jusqu'à ce que la partie s'arrête (fin de partie ou erreur d'une stratégie). */

  /* Affichage du classement final */
  game_engine__report(&gm_eng);

  /* Destruction du game_engine */
  game_engine__cleanup(&gm_eng);

  return gm_eng.gp.error;
}
