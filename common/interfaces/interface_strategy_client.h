#ifndef __INTERFACE_STRATEGY_CLIENT_H__
#define __INTERFACE_STRATEGY_CLIENT_H__

/******************************************************
 * Chaque stratégie cliente implémente ces 3 méthodes
 * qui seront appelées exclusivement par le serveur :
 ******************************************************/

// Initialisation de la stratégie :
//  - appelée une seule fois avant le début de la partie
//  - ne doit pas réaliser d'actions
int api_client__setup(void);

// Exécution de la stratégie :
//  - appelée à chaque tour
//  - doit réaliser exactement une action :
//      soit t le numéro du tour et p le nombre de pingouins par joueur
//		- si t <= p : api_server__place_penguin
//		- sinon : api_server__move_penguin
int api_client__run(void);

// Nettoyage de la stratégie :
//  - appelée une seule fois après la fin de la partie
//  - ne doit pas réaliser d'actions
int api_client__cleanup(void);

#endif
