
#ifndef  __PENGUIN_H__
#define  __PENGUIN_H__

#include <assert.h>

#include "world_conf.h"


struct penguin {
  unsigned int id;
  unsigned int player_id;
  unsigned int penguin_player_id;

  int tile_id;

  int is_blocked;
  /*
  unsigned int score_tiles;
  unsigned int score_fishes;
  unsigned int score_distance;
  */
};


void penguin__init (struct penguin *pg, unsigned int player_id, unsigned int penguin_player_id);
void penguin__copy (struct penguin *pg_dst, struct penguin *pg_src);
void penguin__destroy (struct penguin *pg);

int penguin__is_finished (struct penguin *pg);


#endif
