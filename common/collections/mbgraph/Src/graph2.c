/*
 *  graph2.c - a graph as an adjacency list
 *  Copyright (C) 2010 Martin Broadhurst
 *  www.martinbroadhurst.com
 */

#include <stdlib.h>

#include <edge.h>

#include <graph2.h>

typedef struct {
	MBset *neighbours;
} vertex_body;

static vertex_body * vertex_body_create(void)
{
	vertex_body * body = malloc(sizeof(vertex_body));
	if (body) {
		body->neighbours = MBset_create((MBcmpfn)MBvertex_compare);
	}
	return body;
}

static void vertex_body_delete(vertex_body * body)
{
	if (body) {
		MBset_delete(body->neighbours);
		free(body);
	}
}

static MBset * vertex_get_neighbours(const MBvertex * vertex)
{
	return ((vertex_body*)vertex->body)->neighbours;
}

MBgraph2 *MBgraph2_create(void)
{
	MBgraph2 *graph = malloc(sizeof(MBgraph2));
	if (graph) {
		graph->vertices = MBset_create((MBcmpfn)MBvertex_compare);
	}
	return graph;
}

void MBgraph2_delete(MBgraph2 *graph)
{
	if (graph) {
		MBset_for_each(graph->vertices, (MBforfn)MBvertex_delete);
		MBset_delete(graph->vertices);
		free(graph);
	}
}

MBvertex *MBgraph2_add(MBgraph2 *graph, const char *name, void *data)
{
	MBvertex *vertex = MBvertex_create(name, data, vertex_body_create(), (MBdeletefn)vertex_body_delete);
	MBvertex *existing = MBset_add(graph->vertices, vertex);
	MBvertex_delete(existing);
	return vertex;
}

MBvertex *MBgraph2_get_vertex(const MBgraph2 *graph, const char *name)
{
	return MBset_find(graph->vertices, &name);
}

void *MBgraph2_remove(MBgraph2 *graph, MBvertex *vertex)
{
	void *data = NULL;
	unsigned int v;
	MBvertex *removed;

	/* Remove the vertex from all neighbour lists */
	for (v = 0; v < MBset_get_count(graph->vertices); v++) {
		MBvertex *current = MBset_get(graph->vertices, v);
		if (MBvertex_compare(current, vertex) != 0) {
			MBset_remove(vertex_get_neighbours(current), vertex);
		}
	}
	/* Remove the vertex */
	removed = MBset_remove(graph->vertices, vertex);
	if (removed) {
		data = removed->data;
		MBvertex_delete(removed);
	}

	return data;
}

void MBgraph2_add_edge(MBgraph2 *graph, MBvertex *vertex1, MBvertex *vertex2)
{
	MBset_add(vertex_get_neighbours(vertex1), vertex2);
}

void MBgraph2_remove_edge(MBgraph2 *graph, MBvertex *vertex1, MBvertex *vertex2)
{
	MBset *neighbours = vertex_get_neighbours(vertex1);
	MBvertex *neighbour = MBset_find(neighbours, vertex2);
	if (neighbour) {
		MBset_remove(neighbours, neighbour);
	}
}

unsigned int MBgraph2_get_adjacent(const MBgraph2 *graph, const MBvertex *vertex1, const MBvertex *vertex2)
{
	return MBset_find(vertex_get_neighbours(vertex1), vertex2) != NULL;
}

MBiterator *MBgraph2_get_neighbours(const MBgraph2 *graph, const MBvertex *vertex)
{
	return MBset_iterator(vertex_get_neighbours(vertex));
}

typedef struct {
	const MBgraph2 *graph;
	unsigned int v; /* Index of the current vertex */
	unsigned int n;	/* Index of the current neighbour */
	MBedge *edge;
} edge_iterator;

static edge_iterator *edge_iterator_create(const MBgraph2 *graph)
{
	edge_iterator *it = malloc(sizeof(edge_iterator));
	if (it) {
		it->graph = graph;
		it->edge = malloc(sizeof(MBedge));
		it->v = 0;
		it->n = 0;
	}
	return it;
}

static void edge_iterator_delete(edge_iterator *it)
{
	if (it) {
		free(it->edge);
		free(it);
	}
}

static void *edge_iterator_get(edge_iterator *it)
{
	MBedge *edge = NULL;
	MBvertex *vertex;
	MBvertex *neighbour;

	vertex = MBset_get(it->graph->vertices, it->v);
	while (vertex != NULL && edge == NULL) {
		neighbour = MBset_get(vertex_get_neighbours(vertex), it->n);
		if (neighbour) {
			it->edge->from = vertex;
			it->edge->to = neighbour;
			it->n++;
			edge = it->edge;
		}
		else {
			/* Move to the next vertex */
			it->v++;
			vertex = MBset_get(it->graph->vertices, it->v);
			it->n = 0;
		}
	}

	return edge;
}

MBiterator *MBgraph2_get_edges(const MBgraph2 *graph)
{
	return MBiterator_create(edge_iterator_create(graph), (MBgetfn)edge_iterator_get,
		(MBdeletefn)edge_iterator_delete);
}

MBiterator *MBgraph2_get_vertices(const MBgraph2 *graph)
{
	return MBset_iterator(graph->vertices);
}

unsigned int MBgraph2_get_neighbour_count(const MBgraph2 * graph, const MBvertex * vertex)
{
	return MBset_get_count(vertex_get_neighbours(vertex));
}

unsigned int MBgraph2_get_edge_count(const MBgraph2 * graph)
{
	/* The edge count is the sum of all of the neighbour counts */
	unsigned int v;
	unsigned int edges = 0;
	for (v = 0; v < MBset_get_count(graph->vertices); v++) {
		const MBvertex *vertex = MBset_get(graph->vertices, v);
		edges += MBset_get_count(vertex_get_neighbours(vertex));
	}
	return edges;
}

unsigned int MBgraph2_get_vertex_count(const MBgraph2 * graph)
{
	return MBset_get_count(graph->vertices);
}
