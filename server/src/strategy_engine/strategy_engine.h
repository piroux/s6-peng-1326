
#ifndef  __STRATEGY_ENGINE_H__
#define  __STRATEGY_ENGINE_H__


#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>

#include "log.h"
#include "random.h"

#include "strategy.h"
#include "../game_engine/game_parameters.h"



struct strategy_engine {
  struct game_parameters *gp;
  struct strategy_client **strategies;
};


void strategy_engine__setup(struct strategy_engine *st_eng, struct game_parameters *gp);
void strategy_engine__next(struct strategy_engine *st_eng);
void strategy_engine__cleanup(struct strategy_engine *st_eng);

#endif
