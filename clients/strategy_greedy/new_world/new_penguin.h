
#ifndef  __PENGUIN_H__
#define  __PENGUIN_H__

#include <assert.h>
#include <stdio.h>

#include "graph.h"
#define MAX_PLAYERS 10

#define MAX_PENGUINS_PER_PLAYER 10

#define MAX_TILES 100


struct penguin {
  unsigned int player_id;
  int tile_id;
};


void penguin__construct (struct graph* g, struct penguin *pg, unsigned int player_id, unsigned int penguin_player_id);


#endif
