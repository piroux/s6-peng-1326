
#ifndef  __TESSEL_H__
#define  __TESSEL_H__

#include <stdlib.h>
#include <math.h>
#include <assert.h>

#include "tcoord.h"


enum tessel_kind {
  TESSEL_SQUARE,
  TESSEL_HEXA
};

struct tessel {
  enum tessel_kind kind;
  unsigned int nb_directions;
  unsigned int tiles_per_axis;
  void (*init_tcoord) (struct tessel *t, unsigned int tile_id, struct tcoord *c);
  void (*set_tcoord_for_dir) (struct tessel *t, struct tcoord *tc2, struct tcoord *tc1, unsigned int direction);
  unsigned int (*get_global_dir) (struct tessel *t, struct tcoord *tc2, struct tcoord *tc1);
};


void tessel__init (struct tessel *t, enum tessel_kind tk, unsigned int nb_tiles);

void tessel__copy (struct tessel *t_dst, struct tessel *t_src);


/* Each kind of tessellation has to provide an implementation of 2 methods :
  <tessel_name>__init_tcoord (struct tessel *t, unsigned int tile_id, struct tcoord *c);
    Allow to fill in the tcoord <c> using the tile_id.
  <tessel_name>__set_tcoord_for_dir (struct tessel *t, struct tcoord *tc2, struct tcoord *tc1, unsigned int direction)
    Allow to fill in the tcoord <tc2> using the starting one <tc1> and a global direction.
*/

/* Square tessel */
void tessel_square__init_tcoord (struct tessel *t, unsigned int tile_id, struct tcoord *c);
void tessel_square__set_tcoord_for_dir (struct tessel *t, struct tcoord *tc2, struct tcoord *tc1, unsigned int direction);
unsigned int tessel_square__get_global_dir(struct tessel *t, struct tcoord *tc2, struct tcoord *tc1);

/* Hexa tessel */
void tessel_hexa__init_tcoord (struct tessel *t, unsigned int tile_id, struct tcoord *c);
void tessel_hexa__set_tcoord_for_dir (struct tessel *t, struct tcoord *tc2, struct tcoord *tc1, unsigned int direction);
unsigned int tessel_hexa__get_global_dir(struct tessel *t, struct tcoord *tc2, struct tcoord *tc1);

#endif
