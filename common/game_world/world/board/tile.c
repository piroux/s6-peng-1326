
#include "tile.h"

void tile__init (struct tile *ti, struct tessel *tg) {
  static int nb_tiles = 0;
  ti->id = nb_tiles++;
  assert(nb_tiles <= MAX_TILES);

  ti->state = FREE;

  ti->nb_fish = rand() % MAX_FISH_PER_TILE + 1;

  /* no penguin placed on it */
  ti->penguin_id = -1;

  tg->init_tcoord(tg, ti->id, &ti->coord);
}

void tile__copy (struct tile *ti_dst, struct tile *ti_src) {
  ti_dst->id = ti_src->id;
  ti_dst->state = ti_src->state;
  ti_dst->nb_fish = ti_src->nb_fish;
  ti_dst->penguin_id = ti_src->penguin_id;
  tcoord__copy(&ti_dst->coord, &ti_src->coord);
}


void tile__destroy (struct tile *ti) {
  // ok
}
