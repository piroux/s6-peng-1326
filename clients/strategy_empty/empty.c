
// empty strategy

#include "interface_strategy_client.h"
#include "interface_strategy_server.h"

#include <stdio.h>


unsigned int player_id;

int api_client__setup(void) {
  player_id = api_server__current_player();
  printf("[empty %d=%d] %s\n", player_id, api_server__current_player(), __func__);

  return 0;
}


int api_client__run(void) {
  printf("[empty %d=%d] %s\n", player_id, api_server__current_player(), __func__);

  return 0;
}


int api_client__cleanup(void) {
  printf("[empty %d=%d] %s\n", player_id, api_server__current_player(), __func__);

  return 0;
}
