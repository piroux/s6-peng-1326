
#include "strategy.h"


/* Importateur de stratégie */
struct strategy_client *strategy__import(char *path) {
  struct strategy_client *sc = malloc(sizeof(struct strategy_client));
  char *error;

  if (sc == NULL) {
    perror("import_client: malloc failed");
    exit(EXIT_FAILURE);
  }

  sc->name = path;
  sc->path = path;

  log_debug("Loading strategy : %s", path);

  /* Ouverture de la bibliothèque partagée */
  sc->handle = dlopen(path, RTLD_LAZY);
  if (sc->handle == NULL) {
    log_error("(opening) %s\n", dlerror());
    exit(EXIT_FAILURE);
  }
  dlerror();

  /* Extraction de symbole de la bibliothèque partagée et stockage en tant que
  pointeur de fonction: api_client__setup */
  *(void **) (&sc->api_client__setup) = dlsym(sc->handle, "api_client__setup");
  if ((error = dlerror()) != NULL) {
    log_error("(sym) %s\n", error);
    exit(EXIT_FAILURE);
  }

  /* Extraction de symbole de la bibliothèque partagée et stockage en tant que
  pointeur de fonction: api_client__run */
  *(void **) (&sc->api_client__run) = dlsym(sc->handle, "api_client__run");
  if ((error = dlerror()) != NULL) {
    log_error("(sym) %s\n", error);
    exit(EXIT_FAILURE);
  }

  /* Extraction de symbole de la bibliothèque partagée et stockage en tant que
  pointeur de fonction: api_client__cleanup */
  *(void **) (&sc->api_client__cleanup) = dlsym(sc->handle, "api_client__cleanup");
  if ((error = dlerror()) != NULL) {
    log_error("(sym) %s\n", error);
    exit(EXIT_FAILURE);
  }

  return sc;
}
