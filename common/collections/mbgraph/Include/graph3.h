/*
 *  graph3.h - a graph as an adjacency matrix
 *  Copyright (C) 2010 Martin Broadhurst 
 *  www.martinbroadhurst.com
 */

#ifndef GRAPH3_H
#define GRAPH3_H

#include <vertex.h>
#include <edge.h>
#include <set.h>
#include <matrix.h>
#include <iterator.h>

typedef struct {
	MBset    * vertices;
	MBmatrix * edges;
} MBgraph3;

MBgraph3 *   MBgraph3_create(void);
void         MBgraph3_delete(MBgraph3 *graph);
MBvertex *   MBgraph3_add(MBgraph3 *graph, const char *name, void *data);
MBvertex *   MBgraph3_get_vertex(const MBgraph3 *graph, const char *name);
void *       MBgraph3_remove(MBgraph3 *graph, MBvertex *vertex);
void         MBgraph3_add_edge(MBgraph3 *graph, MBvertex *vertex1, MBvertex *vertex2);
void         MBgraph3_remove_edge(MBgraph3 *graph, MBvertex *vertex1, MBvertex *vertex2);
unsigned int MBgraph3_get_adjacent(const MBgraph3 *graph, const MBvertex *vertex1, const MBvertex *vertex2);
MBiterator * MBgraph3_get_neighbours(const MBgraph3 *graph, const MBvertex *vertex);
MBiterator * MBgraph3_get_edges(const MBgraph3 *graph);
MBiterator * MBgraph3_get_vertices(const MBgraph3 *graph);
unsigned int MBgraph3_get_neighbour_count(const MBgraph3 * graph, const MBvertex * vertex);
unsigned int MBgraph3_get_edge_count(const MBgraph3 * graph);
unsigned int MBgraph3_get_vertex_count(const MBgraph3 * graph);

#endif /* GRAPH3_H */