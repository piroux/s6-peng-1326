
#ifndef ____plateau__
#define ____plateau__

#include <stdio.h>
#include <stdlib.h>
#include "new_player.h"

struct world {
    /* World objects */
    struct player players[MAX_PLAYERS];
    struct graph g;
    
    /* World configuration */
    int nb_players;
    int nb_penguins_per_player;
};



struct world* world__construct();
void world_free(struct world* w);

#endif /* defined(____plateau__) */
