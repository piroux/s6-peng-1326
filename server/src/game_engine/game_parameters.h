
#ifndef __GAME_PARAMETERS_H__
#define __GAME_PARAMETERS_H__

#include "tessel.h"
#include "world_conf.h"

/*
#define USE_LOG 1
#define USER_BARRIERS 1
*/

/* Settings for game : */

/* Types of tesselation */
#define TESSEL_SHAPE TESSEL_HEXA /* TESSEL_HEXA or TESSEL_SQUARE */

/* Nombre de pingouins à placer pour chaque joueur */
#define NB_PENGUINS_PER_PLAYER 3

/* Nombre de cases pour le plateau de jeu */
#define NB_TILES 60


struct game_parameters {
  /* features configuration */
  //++ int use_mod_ui;
  //++ int use_mod_stats;

  /* current game state */
  char **strategies_paths;
  int nb_strategies;
  int current_strategy;

  /* final state */
  int error;
  int winner;
};

#endif
