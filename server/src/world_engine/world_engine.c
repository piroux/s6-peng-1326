
#include "world_engine.h"


/* World Engine : Gestionnaire du modèle de données du jeu.


  Ce composant est responsable de la gestion des données du jeu.
  Il est l'interface de haut-niveau faisant évoluer le modèle du jeu au fur et
  à mesure que la partie se déroule.

  Ce gestionnaire est composé de trois éléments majeurs :


  1. Il contient donc le game_world qui est une macro structure représentant le
  modèle du jeu à un instant donné, et la logique de jeu, qui est utilisée
  dans le game_engine pour décider de l'application des actions enregistrées du
  joueur, ainsi que de la fin de partie par exemple.

  Le world_engine utilise un double modèle de jeu :
    - un modèle original pour la gestion interne
    - un modèle synchronisé avec avec le premier et écrasé à chaque début de
    tour, sur lequel sont appliqués les méthodes de l'interface (api_server__*).

  Ce dédoublage permet de s'assurer que les fonctions de l'interface ne
  modifient pas les données du jeu, quelque soit la qualité de leur
  implémentation. En soi, cette fonctionnalité n'est pas indispensable et il
  aurait été tout à fait possible de ne pas l'implémenter. Cependant elle
  constitue un élément de sécurité volontairement redondant dans le cadre d'un
  projet où la sécurité des données du modèle de jeu dans le serveur est
  prioritaire sur la performance.


  2. Il possède aussi un modèle d'action (game_action) qui doit enregistrer le
  type et les paramètres des actions de placement et de déplacement lors des
  appels au fonctions de l'interface de programmation à chaque tour du jeu.


  3. Il possède également un drapeau (world_state) permettant de savoir :
    - si la partie peut être poursuivie : WORLD_PLAYING,
    - si la partie doit se terminer à cause de l'erreur d'une stratégie : WORLD_ERROR,
    - si la partie doit se terminer car tous les pingouins sont bloqués : WORLD_OVER,

*/


/* Constructeur pour le world_engine */
void world_engine__setup (struct world_engine *wd_eng, struct game_parameters *gp) {
  log_debug("started", NULL);

  /* Inialisation des protections mémoires */
  bzero((void *) wd_eng->barrier_1, BARRIER_LENGTH * sizeof(char));
  bzero((void *) wd_eng->barrier_2, BARRIER_LENGTH * sizeof(char));
  bzero((void *) wd_eng->barrier_3, BARRIER_LENGTH * sizeof(char));

  wd_eng->gp = gp;

  // TODO : make generic : 3, 60, TESSEL_HEXA

  /* Initialisation du game_world d'après les paramètres fournis sur la ligne de commande */
  game_world__init(&wd_eng->world_backend, gp->strategies_paths, gp->nb_strategies, NB_PENGUINS_PER_PLAYER, NB_TILES, TESSEL_SHAPE);
  game_action__init(&wd_eng->action);

  /* Copie conforme du game_world original vers le game_world d'interface */
  wd_eng->world_frontend.w.grid_board.graph = NULL;
  game_world__copy(&wd_eng->world_frontend, &wd_eng->world_backend);

  /* Affectation du game_world de l'interface */
  game_world_server__attach(&wd_eng->world_frontend, &wd_eng->action);

  finish_reason = "Not yet finished.";

  wd_eng->state = WORLD_PLAYING;

  log_debug("finished", NULL);
}


/* Destructeur pour le world_engine */
void world_engine__cleanup (struct world_engine *wd_eng) {
  log_debug("started", NULL);

  /* Destruction des deux modèles de jeu : original et interface */
  game_world__destroy(&wd_eng->world_backend);
  game_world__destroy(&wd_eng->world_frontend);

  log_debug("finished", NULL);
}


/* Vérificateur pour une barrière mémoire */
int check_barrier (char *barrier, int n) {
  for (int i = 0; i < n; i++) {
    if (barrier[i] != '\0') {
      printf("%s : barrier[%d] infringed : %c != \\0 \n",
        __func__, i, barrier[i]);
      return 0;
    }
  }
  return 1;
}


/* Vérificateur des protections mémoires internes  */
int world_engine__check (struct world_engine *wd_eng) {

  return (
    check_barrier(wd_eng->barrier_1, BARRIER_LENGTH) &&
    check_barrier(wd_eng->barrier_2, BARRIER_LENGTH) &&
    check_barrier(wd_eng->barrier_3, BARRIER_LENGTH));
}


/* Récupérateur d'erreurs des stratégies clientes

  Renvoie
    1 si une erreur de stratégie avait été auparavant détectée,
    0 sinon
*/
int world_engine__check_error (struct world_engine *wd_eng) {
  log_debug("", NULL);

  return (wd_eng->state == WORLD_ERROR);
}


/*  Vérificateur de fin de partie d'après le modèle de jeu

  Renvoie
    1 si le modèle ne doit pas continuer à évoluer,
    0 sinon
*/
int world_engine__is_finished (struct world_engine *wd_eng) {
  log_debug("", NULL);

  return (wd_eng->state != WORLD_PLAYING);
}


/* Préparateur du modèle du jeu pour le tour en cours */
void world_engine__next (struct world_engine *wd_eng) {
  log_debug("started", NULL);

  /* Ecrasage du modèle d'interface par le modèle original */
  game_world__copy(&wd_eng->world_frontend, &wd_eng->world_backend);

  /* Réinitialisation de l'action pour le tour en cours */
  game_action__reset(&wd_eng->action);

  log_debug("finished", NULL);
}


/*  Vérificateur de joueur non bloqué

  Renvoie
    1 si le modèle du jeu détermine que le joueur peut continuer à jouer,
    cad qu'au moins un de ses pingouins peut encore bouger,
    0 sinon
*/
int world_engine__can_play (struct world_engine *wd_eng) {
  return game_world__current_player_can_play(&wd_eng->world_backend);
}


/* Applicateur de l'action enregistrée

  L'action est appliquée d'après la commande de mouvenent (place ou move)
  lors du dernier tour de la stratégie
*/
void world_engine__apply_action (struct world_engine *wd_eng) {
  log_debug("started", NULL);

  int action_valid = game_world__check_action(&wd_eng->world_backend, &wd_eng->action, world_engine__set_finish_reason);

  /* Si l'action est bien valide et que la stratégie peut jouer,
  l'action est appliquée au game_world original. */
  if (action_valid > 0) {
    game_world__apply_action(&wd_eng->world_backend, &wd_eng->action);
    log_debug("action valid: applied (%d)", action_valid);
  }
  /* Sinon si l'action est valide, mais que la stratégie ne peut plus
  jouer, l'action n'est pas appliquée. */
  else if (action_valid < 0) {
    log_debug("no action: finished (%d)", action_valid);
  }
  /* Sinon l'action n'est pas valide, la stratégie est déclarée
  perdante et l'erreur est signalée pour arrêter la partie. */
  else {
    log_error("action invalid: canceling (%d)", action_valid);
    game_world__nuke_current_player(&wd_eng->world_backend);
    wd_eng->state = WORLD_ERROR;
  }

  /* Classement des joueurs avec les nouvelles données du modèle de jeu */
  game_world__rank_score_players(&wd_eng->world_backend);

  log_debug("finished", NULL);
}


/* Préparateur du tour suivant en faisant itérer le modèle du jeu
et véérifiant que chaque stratégie peut encore jouer */
void world_engine__prepare_next (struct world_engine *wd_eng) {
  game_world__prepare_next_round(&wd_eng->world_backend);

  game_world__check_blocked_penguins(&wd_eng->world_backend);

  if (game_world__is_finished(&wd_eng->world_frontend)) {
    wd_eng->state = WORLD_OVER;
  }
}


/*  Afficheur du classement final en fin de partie

  Renvoie l'identifiant de la meilleure stratégie. */
int world_engine__report_rankings (struct world_engine *wd_eng) {
  if (finish_reason != NULL) {
    fprintf(stdout, "Game finished : %s\n", finish_reason);
  }

  fprintf(stdout, "Nb rounds : %u\n", wd_eng->world_backend.current_round);

  return game_world__report_rankings(&wd_eng->world_backend);
}


/* Enregistreur de la raison de fin de partie */
void world_engine__set_finish_reason (char* reason) {
  log_msg(WARNING, "game_world__check_action", "%s", reason);
  finish_reason = reason;
}
