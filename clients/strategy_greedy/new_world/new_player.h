

#ifndef  __PLAYER_H__
#define  __PLAYER_H__

#include "new_penguin.h"
#include <stdio.h>


struct player {
  unsigned int id;
  // the pings and the id of each one
  struct penguin penguins[MAX_PENGUINS_PER_PLAYER]; 
  unsigned int nb_penguins_max;
};


void player__construct (struct graph* g, struct player *pl, int id_player, int nb_penguins_per_player);
#endif
