/*
 *  graph4.h - a graph as an incidence matrix
 *  Copyright (C) 2010 Martin Broadhurst 
 *  www.martinbroadhurst.com
 */


#ifndef GRAPH4_H
#define GRAPH4_H

#include <vertex.h>
#include <edge.h>
#include <set.h>
#include <matrix.h>
#include <iterator.h>

typedef struct {
	MBset    * vertices;
	MBmatrix * edges;
} MBgraph4;

MBgraph4 *   MBgraph4_create(void);
void         MBgraph4_delete(MBgraph4 *graph);
MBvertex *   MBgraph4_add(MBgraph4 *graph, const char *name, void *data);
MBvertex *   MBgraph4_get_vertex(const MBgraph4 *graph, const char *name);
void *       MBgraph4_remove(MBgraph4 *graph, MBvertex *vertex);
void         MBgraph4_connect(MBgraph4 *graph, MBvertex *vertex1, MBvertex *vertex2);
void         MBgraph4_add_edge(MBgraph4 *graph, MBvertex *vertex1, MBvertex *vertex2);
void         MBgraph4_add_edge(MBgraph4 *graph, MBvertex *vertex1, MBvertex *vertex2);
void         MBgraph4_remove_edge(MBgraph4 *graph, MBvertex *vertex1, MBvertex *vertex2);
unsigned int MBgraph4_get_adjacent(const MBgraph4 *graph, const MBvertex *vertex1, const MBvertex *vertex2);
MBiterator * MBgraph4_get_neighbours(const MBgraph4 *graph, const MBvertex *vertex);
MBiterator * MBgraph4_get_edges(const MBgraph4 *graph);
MBiterator * MBgraph4_get_vertices(const MBgraph4 *graph);
unsigned int MBgraph4_get_neighbour_count(const MBgraph4 * graph, const MBvertex * vertex);
unsigned int MBgraph4_get_edge_count(const MBgraph4 * graph);
unsigned int MBgraph4_get_vertex_count(const MBgraph4 * graph);

#endif /* GRAPH4_H */