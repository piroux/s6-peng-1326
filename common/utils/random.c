
#include "random.h"


void seed_random(unsigned value) {
  if (value == 0) {
    srand(time(NULL));
  }
  else {
    srand(value);
  }
}


int random_integer(int max) {
  if (max <= 0) {
    max = RAND_MAX;
  }
  return rand() % max;
}


float random_float(float max) {
  return max * ((float)rand() / RAND_MAX);
}


int random_boolean() {
  return rand() % 2;
}
