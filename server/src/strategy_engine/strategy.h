
#ifndef  __STRATEGY_CLIENT_H__
#define  __STRATEGY_CLIENT_H__

#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>

#include "log.h"


struct strategy_client {
  char *name;
  char *path;
  void *handle;

  int (*api_client__setup)(void);
  int (*api_client__run)(void);
  int (*api_client__cleanup)(void);

  int run;
};


struct strategy_client *strategy__import(char *path);

#endif
