#include "log.h"


void log_fatal(char* msg) {
  fprintf(stderr, "#[ERROR] %s\nExit!\n", msg);
  fflush(stderr);
  exit(-1);
}

/*
void debug_lite(char* msg) {
  #if DEBUG
  fprintf(stderr, "#[DEBUG] %s\n", msg);
  fflush(stderr);
  #endif
}
*/

int ok_log(enum log_level level) {
  return (USE_LOG && level >= LOG_LEVEL);
}


void log_msg(enum log_level level_id, const char *func, const char *fmt, ...) {
  #if USE_LOG

  if (level_id >= LOG_LEVEL) {
    va_list arglist;

    static char* level_names[] = {
      "TRACE",    // white
      "DEBUG",    // light blue
      "INFO",     // ligth green
      "WARNING",  // yellow
      "ERROR",    // orange
      "CRITICAL", // red
    };

    assert(level_id < LOG_LEVELS);
    /*
    //time_t current_time;
    struct tm *time_info;
    char time_string[9];

    long            ms; // Milliseconds
    //time_t          s;  // Seconds
    struct timespec spec;

    //clock_gettime(CLOCK_REALTIME, &spec);

    //s  = spec.tv_sec;
    ms = round(spec.tv_nsec / 1.0e6);

    //time(&current_time);
    time_info = localtime(&spec.tv_sec);

    strftime(time_string, sizeof(time_string), "%T", time_info);

    fprintf(stderr, "#[%s %s.%03ld] ", level_names[level_id], time_string, ms);
    */

    fprintf(stderr, "#[%s] ", level_names[level_id]);
    if (func != NULL) {
      fprintf(stderr, "(%s) ", func);
    }
    va_start(arglist, fmt);
    vfprintf(stderr, fmt, arglist);
    va_end(arglist);
    fprintf(stderr, "\n");
  }

  #else

  (void) fmt;

  #endif
}
