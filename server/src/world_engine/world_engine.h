
#ifndef  __WORLD_ENGINE_H__
#define  __WORLD_ENGINE_H__

#include <stdio.h>
#include <strings.h>

#include "log.h"

#include "../game_engine/game_parameters.h"
#include "game_world.h"
#include "game_action.h"


#define BARRIER_LENGTH (1 << 10)


enum world_state {
  WORLD_PLAYING,
  WORLD_ERROR,
  WORLD_OVER,
};


struct world_engine {
  struct game_parameters *gp;

  char barrier_1[BARRIER_LENGTH];
  struct game_world world_backend;  // original
  char barrier_2[BARRIER_LENGTH];
  struct game_world world_frontend; // copy
  char barrier_3[BARRIER_LENGTH];

  struct game_action action;

  enum world_state state;
};

char *finish_reason;

void world_engine__setup (struct world_engine *wd_eng, struct game_parameters *gp);
void world_engine__next (struct world_engine *wd_eng);
void world_engine__cleanup (struct world_engine *wd_eng);
//void world_engine__set_context(struct world_engine *w, enum play_context pc);

void world_engine__prepare_next (struct world_engine *wd_eng);

//struct game_world *world_engine__get_world(struct world_engine *wd_eng);

int world_engine__is_finished (struct world_engine *wd_eng);
int world_engine__check (struct world_engine *wd_eng);
int world_engine__check_error (struct world_engine *wd_eng);

int world_engine__can_play (struct world_engine *wd_eng);

void world_engine__apply_action (struct world_engine *wd_eng);

void game_world_server__attach (struct game_world *_gw, struct game_action *_ga);

void world_engine__set_finish_reason (char* reason);

int world_engine__report_rankings (struct world_engine *wd_eng);

#endif
