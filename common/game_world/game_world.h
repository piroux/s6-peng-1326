
#ifndef  __GAME_WORLD_H__
#define  __GAME_WORLD_H__

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "log.h"
#include "world/world.h"
#include "world/board/grid.h"
#include "world/board/tessel.h"
#include "world/world_conf.h"
#include "game_action.h"


struct game_world {

  /* Le "World" : sous-modèle de données du jeu */
  struct world w;


  /* Etat courant du modèle de jeu : */

  /* Joueur courant */
  unsigned int current_player_id;

  /* Numéro du tour actuel */
  unsigned int current_round;

  /* Classement des joueurs */
  struct player *rankings[MAX_PLAYERS];
};


void game_world__init (struct game_world *gw, char **player_names, int nb_players, int nb_penguins_per_player, int nb_tiles, enum tessel_kind tk);
void game_world__copy (struct game_world *gw_dst, struct game_world *gw_src);
void game_world__destroy (struct game_world *gw);

void game_world__print (struct game_world *gw);

int game_world__apply_action (struct game_world *gw, struct game_action *ga);

int game_world__check_action (struct game_world *gw, struct game_action *ga, void (*register_action_failure_) (char *));

void game_world__prepare_next_round (struct game_world *gw);

int game_world__current_player_can_play (struct game_world *gw);

void game_world__nuke_current_player (struct game_world *gw);

void game_world__rank_score_players (struct game_world *gw);

void game_world__check_blocked_penguins (struct game_world *gw);

int game_world__is_finished (struct game_world *gw);

int game_world__report_rankings (struct game_world *gw);


#endif
