
#ifndef ____parcours__
#define ____parcours__

#include <stdio.h>
#include <stdlib.h>
#include "new_world.h"

int alone_rec (struct world* w, int id_tile, int* colours);
int* alone (struct world* w, int id_tile);
int* parcours(struct world* w, int* colours, int id_tile);
int parcours_rec (struct world* w, int* number, int* pere, int id_tile,int depth);
#endif /* defined(____parcours__) */
