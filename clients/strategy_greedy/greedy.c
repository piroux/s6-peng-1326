
#include "greedy.h"



/*
  PLACEMENT STRATEGY
*/


// Sum the number of fish from neighboring tiles tile_id
int nb_fish_arround(unsigned int tile_ID){

  unsigned int dir = api_server__nb_directions(tile_ID);
  int fish = 0;
  for(unsigned int i = 0; i < dir ; i++){
    if(api_server__is_free(api_server__get_tile (tile_ID, i, 1)) != 0){
      unsigned int vois = api_server__get_tile(tile_ID,i,1);
      unsigned int f = api_server__nb_fish(vois);
      fish += f;
    }
  }
  return fish;
}



// Place a penguin on the tile that has the sum of the maximum neighboring tiles
int place(){

    unsigned int nb_start = api_server__nb_starting_tiles();
    int max = 0;
    unsigned int tile = api_server__get_starting_tile(0);
    for(unsigned int i = 0; i < nb_start ; i++){

        unsigned int tile_tested = api_server__get_starting_tile(i);
        if(api_server__nb_fish(tile_tested) == 1){
            int maxi = nb_fish_arround(tile_tested);
            if(maxi>max){
                max = maxi;
                tile = tile_tested;
            }
        }
    }
    api_server__place_penguin(tile);
    return EXIT_SUCCESS;
}

/*
  DISPLACEMENT STRATEGY
*/


// Initialize a structure interesting_tile
void interesting_tile__init (struct interesting_tile* t, unsigned int id, unsigned int dist, unsigned int type){

  assert ( t != NULL);
  t->id = id;
  t->from_dir = type;
  t->from_dist = dist;
  t->score_adv = 0;
  t->score_fish = 0;
}





// Find accessible tiles in peng_tile with nb_fish fishs and create a board of struct interesting_tile which contains all structure interesting_tile of id accessible tiles finding
void access_tiles_n_fish (unsigned int peng_tile, int nb_dir, int nb_fish,  struct interesting_tile** interesting_tiles){
    
  assert(nb_fish < 4);

  unsigned int tile_acc;
  int count = 0;
  for (int d = 0; d < nb_dir; d++){
    if(api_server__is_free(api_server__get_tile (peng_tile, d, 1)) != 0){
      for (int i = 1; (i < MAX_TILES); i++){
	tile_acc = api_server__get_tile(peng_tile,d,i);

	//all tiles after are not accessible in the same direction
	if(!api_server__is_free(tile_acc))
	  break;

	else{
	  if(api_server__nb_fish(tile_acc) == nb_fish){
	    struct interesting_tile* t = malloc(sizeof(struct interesting_tile));
	    interesting_tile__init(t,tile_acc, i, d);
		    
	    // initialize the board
	    interesting_tiles[count] = t;
	    count++;
	     
	  }
	 
	}
      }
    }
    if(count > MAX_INTERESTING_TILE)
      break;
  }
  if(count == 0)
    interesting_tiles[0] = NULL;
}



// For a struct interesting_tile t, count the number of advesary which have access to that tile and save in t->score_adv
void access_tile_adversaries (int nb_dir, int nb_peng, unsigned int current_player, struct interesting_tile* t){

  assert( t != NULL);
  unsigned int tile;
  for (int d = 0; d < nb_dir; d++){
    if(api_server__is_free(api_server__get_tile (t->id, d, 1)) != 0){
      for (int i = 1; i< MAX_TILES; i++){
	tile = api_server__get_tile(t->id,d,i);

	if(!api_server__is_free(tile)){

	  //if the penguin on tile is mine, score_adv is not incremen
	  int mine = 1;
	  for(int j = 0; j<api_server__nb_players(); j++){
	    if (j != current_player){
	      for (int k = 0; k<nb_peng; k++) {
		if (api_server__get_penguin_tile (j, k) == tile)
		  mine = 0;
	      }
	    }
	  }
	  if(mine == 0)
	    t->score_adv += 1;

	  //all tiles after are not accessible by t
	  break;
	  
	}
      }
    }
  }
}






// recursive function which increment the score_fish and the score_adv of a struct interesting tile t with the score_fish and score_adv of the best struct interesting tile if the penguin is in the tile t->id and find the best move
struct interesting_tile best_move_rec (unsigned int current_player, unsigned int peng_tile, int nb_dir, int nb_peng, int depth){
 int nb_fish = 3;
    
  struct interesting_tile** interesting_tiles = malloc(sizeof(int)*MAX_INTERESTING_TILE);

  access_tiles_n_fish(peng_tile, nb_dir, nb_fish,interesting_tiles);
  //looking for accessible tiles with the greatest number of fish
  while (0 < nb_fish && interesting_tiles[0] == NULL){
    nb_fish--;
    access_tiles_n_fish(peng_tile, nb_dir, nb_fish,interesting_tiles);
  }

  struct interesting_tile best_tile = *interesting_tiles[0];  


  //parcours of interesting tiles
  for(int i = 0;i< MAX_INTERESTING_TILE ;i++){
    access_tile_adversaries(nb_dir, nb_peng, current_player, interesting_tiles[i]);
    interesting_tiles[i]->score_fish = nb_fish;
    nb_dir = api_server__nb_directions (interesting_tiles[i]->id); 


    if( depth != 0){
      struct interesting_tile t = best_move_rec(current_player,interesting_tiles[i]->id,nb_dir,nb_peng,depth-1);
      interesting_tiles[i]->score_fish += t.score_fish;
      interesting_tiles[i]->score_adv += t.score_adv; 
    }
  }
    
   
  for(int i = 0; i< MAX_INTERESTING_TILE;i++){
    if (best_tile.score_fish < interesting_tiles[i]->score_fish || (best_tile.score_fish == interesting_tiles[i]->score_fish && best_tile.score_adv < interesting_tiles[i]->score_adv))
      best_tile = *interesting_tiles[i];
    
  }
  return best_tile;
}



// Find the best move with a depth of 3
struct interesting_tile best_move (unsigned int current_player, unsigned int peng_tile, int nb_dir, int nb_peng){
  int nb_fish = 3;
    
  struct interesting_tile** interesting_tiles = malloc(sizeof(int)*MAX_INTERESTING_TILE);

  access_tiles_n_fish(peng_tile, nb_dir, nb_fish,interesting_tiles);
  //looking for accessible tiles with the greatest number of fish
  while (0 < nb_fish && interesting_tiles[0] == NULL){
    nb_fish--;
    access_tiles_n_fish(peng_tile, nb_dir, nb_fish,interesting_tiles);
  }

  int depth = DEPTH;
  struct interesting_tile best_tile = *interesting_tiles[0];  


  //parcours of interesting tiles
  for(int i = 0;i< MAX_INTERESTING_TILE ;i++){
    access_tile_adversaries(nb_dir, nb_peng, current_player, interesting_tiles[i]);
    interesting_tiles[i]->score_fish = nb_fish;
    nb_dir = api_server__nb_directions (interesting_tiles[i]->id); 


 
      struct interesting_tile t = best_move_rec(current_player,interesting_tiles[i]->id,nb_dir,nb_peng,depth);
      interesting_tiles[i]->score_fish += t.score_fish;
      interesting_tiles[i]->score_adv += t.score_adv; 
    
  }
    
   
  for(int i = 0; i< MAX_INTERESTING_TILE;i++){
    if (best_tile.score_fish < interesting_tiles[i]->score_fish || (best_tile.score_fish == interesting_tiles[i]->score_fish && best_tile.score_adv < interesting_tiles[i]->score_adv))
      best_tile = *interesting_tiles[i];
    
  }
  return best_tile;
}


//move the penguin with a best_move with the best score_adv and the best score_fish 
int move(unsigned int current_player,unsigned int nb_peng){
    
  struct interesting_tile best_tile;
  best_tile.score_fish = -1;
  best_tile.score_adv = -1;
  int best_peng;
   
  for (int peng=0; peng<nb_peng; peng++) {
    unsigned int peng_tile = api_server__get_penguin_tile (current_player, peng);
    unsigned int nb_dir = api_server__nb_directions (peng_tile);

    if(nb_dir != 0){
      for(int d = 0;d<nb_dir;d++){
	int ok = 0;
	if(api_server__is_free(api_server__get_tile (peng_tile, d, 1)) != 0)
	  ok = 1;
	if (ok == 1){
	  struct interesting_tile t = best_move(current_player, peng_tile, nb_dir, nb_peng);
      
	  if (best_tile.score_fish < t.score_fish || (best_tile.score_fish == t.score_fish && best_tile.score_adv < t.score_adv)){
	    best_tile = t;
	    best_peng = peng;
	  }
	}
      }
    }
  }
  if(  best_tile.score_adv == -1){
    move_peng(current_player,nb_peng);
  }else{
    api_server__move_penguin(best_peng, best_tile.from_dir, best_tile.from_dist);
  }
  return EXIT_SUCCESS;
}

int move_peng(unsigned int current_player, int nb_peng ) {
 
  int peng = 0;
  unsigned int peng_tile = api_server__get_penguin_tile (current_player, peng);
  unsigned int nb_dir = api_server__nb_directions (peng_tile); 
  unsigned int dir = rand() % nb_dir;
  while(peng<nb_peng && nb_dir == 0){
    nb_dir = api_server__nb_directions (peng_tile);
    unsigned int peng_tile = api_server__get_penguin_tile (current_player, peng);
    dir = rand() % nb_dir;
    while(api_server__is_free(api_server__get_tile (peng_tile, dir, 1)) == 0){
      dir = rand() % nb_dir;
    }
  }
  api_server__move_penguin(peng, dir, 1);

  return EXIT_SUCCESS;
}

int api_client__setup() {return 0;}
int api_client__cleanup() {return 0;}

int api_client__run(){
  unsigned int current_player = api_server__current_player();
  int nb_peng = api_server__nb_penguins(current_player);
  int nb_peng_placed = api_server__nb_penguins_placed (current_player);

  if((nb_peng - nb_peng_placed) > 0) {
    place();
  } else {
     move(current_player,nb_peng);
  }
  return EXIT_SUCCESS;
}



