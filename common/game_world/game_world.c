
#include "game_world.h"

/* Game World : Modèle principal des données du jeu

  Ce composant représente l'état du jeu à un instant donné.

  Il fournit au serveur les méthodes suivantes au serveur :


  1. Des méthodes d'initialisation et de copie du modèle de données. L'existence
  de la méthode de copie est justifiée par :
    - la volonté d'avoir modèle de données redondant dans le serveur,
    - le besoin de pouvoir sérialiser le modèle de données pour des processus
    externes (non implémentés) :
      - gestionnaire d'interface graphique
      - déplacement de l'exécution des stratégies dans d'autres processus.


  2. Méthode de validation d'action. L'action ne sera valide que si une
  stratégie a fourni des paramètres valides et qu'elle a demandé
  exactement une action pendant son tour.


  3. Méthode d'application de l'action. Si l'action est valide, il sera
  possible de l'appliquer au modèle de données du jeu. Cette méthode fait
  largement appel aux autres sous-structures du modèle de données, car elle
  doit mettre à jour pour un placement/déplacement :
    - le joueur auteur de l'action
    - le pingouin déplacé
    - le plateau de jeu
    Le Score est reflété correctement à la fin de chaque application d'action.


  4. Des méthodes d'actualisation et de vérification de joueurs et de pingouins
  bloqués, pour savoir si un joueur devra continuer à jouer, ou encore si la
  partie doit se terminer.


  5. Méthode d'affichage du classement final des différents scores après
  les avoir triés.

*/


/* Initialisateur de game_world */
void game_world__init (struct game_world *gw, char **player_names, int nb_players, int nb_penguins_per_player, int nb_tiles, enum tessel_kind tk) {
  world__init(&gw->w, player_names, nb_players, nb_penguins_per_player, nb_tiles, tk);

  gw->current_player_id = 0;
  gw->current_round = 0;

  for (int pl_id = 0; pl_id < gw->w.nb_players; pl_id++) {
    gw->rankings[pl_id] = &gw->w.players[pl_id];
  }
}


/* Méthode de copie de game_world */
void game_world__copy (struct game_world *gw_dst, struct game_world *gw_src) {

  // warning: beware of pointers !!
  world__copy(&gw_dst->w, &gw_src->w);

  gw_dst->current_player_id = gw_src->current_player_id;
  gw_dst->current_round = gw_src->current_round;

  for (int i = 0; i < gw_dst->w.nb_players; i++) {
    gw_dst->rankings[i] = &gw_dst->w.players[gw_src->rankings[i]->id];
  }
}


/* Destructeur pour le game_world */
void game_world__destroy (struct game_world *gw) {
  world__destroy(&gw->w);
}


/* Afficheur pour le game_world */
void game_world__print (struct game_world *gw) {
  fprintf(stderr, "Game World :\n");
  fprintf(stderr, " * current_round: %u\n", gw->current_round);
  fprintf(stderr, " * current_player_id: %u\n", gw->current_player_id);
  fprintf(stderr, " + world:\n");
  fprintf(stderr, "\t # nb_players: %u\n", gw->w.nb_players);
  fprintf(stderr, "\t # nb_penguins_per_player: %u\n", gw->w.nb_penguins_per_player);
  fprintf(stderr, "\t + players:\n");
  for (int pl_id = 0; pl_id < gw->w.nb_players; pl_id++) {
  fprintf(stderr, "\t\t - player[%u] - %s\n", gw->w.players[pl_id].id, gw->w.players[pl_id].name);
  fprintf(stderr, "\t\t   nb_penguins (placed/max): %u/%u\n",
      gw->w.players[pl_id].nb_penguins_placed,
      gw->w.players[pl_id].nb_penguins_max);
  fprintf(stderr, "\t\t   score: %u/%u/%u\n",
      gw->w.players[pl_id].score_fishes,
      gw->w.players[pl_id].score_tiles,
      gw->w.players[pl_id].score_distance);
  fprintf(stderr, "\t\t   + penguins:\n");
    for(int pg_id = 0; pg_id < gw->w.players[pl_id].nb_penguins_placed; pg_id++) {
  fprintf(stderr, "\t\t\t   - penguin[%u/%u](%u):\n",
        gw->w.players[pl_id].penguins[pg_id].penguin_player_id,
        gw->w.players[pl_id].nb_penguins_placed,
        gw->w.players[pl_id].penguins[pg_id].id);
  fprintf(stderr, "\t\t\t     tile_id: %u\n", gw->w.players[pl_id].penguins[pg_id].tile_id);
  fprintf(stderr, "\t\t\t     blocked: %d\n", gw->w.players[pl_id].penguins[pg_id].is_blocked);
    }

  }
  fprintf(stderr, "\t + rankings: [ ");
  for (int i = 0; i < gw->w.nb_players; i++) {
    fprintf(stderr, "%u ", gw->rankings[i]->id);
  }
  fprintf(stderr, "]\n");
  fprintf(stderr, "\t + grid:\n");
  grid__print(&gw->w.grid_board);
}


void register_action_failure__noop (char *msg) {
  /* register_action_failure fallback for game_world__check__action */
  /* NOOP */
}


/* Vérificateur de alidité de l'action

  Renvoie
    +1  si l'action est valide et peut être appliquée,
    -1  si l'action est valide, mais ne doit pas être appliquée,
     0  sinon : l'action est invalide et ne doit pas être appliquée.
*/
int game_world__check_action (struct game_world *gw, struct game_action *ga, void (*register_action_failure_) (char *)) {
  void (*register_action_failure) (char *) = (register_action_failure_ != NULL) ? register_action_failure_ : register_action_failure__noop;

  log_debug("", NULL);
  /* Lors du dernier tour d'exécution de la stratégie : */

  /* Si la stratégie n'a pas demandé d'action, */
  if (ga->count == 0) {
    assert(ga->kind == ACTION_NOP);
    /* et si tous ses pingouins sont bloqués, c'est normal. */
    if (player__is_finished(&gw->w.players[gw->current_player_id])) {

      register_action_failure("Strategy finished.");
      return -1;
    }
    /* sinon c'est une erreur. */
    else {
      register_action_failure("The strategy did not choose any action.");
      return 0;
    }
  }

  /* Sinon si la stratégie a demandé plusieurs actions, c'est une erreur. */
  if (ga->count > 1) {
    assert(ga->kind == ACTION_PLACE || ga->kind == ACTION_MOVE);
    register_action_failure("The strategy did choose more than one action.");
    return 0;
  }

  /* Sinon si la stratégie a demandé un placement de pingouin : */
  if (ga->kind == ACTION_PLACE) {
    unsigned int player_id = gw->current_player_id;
    unsigned int tile_id = ga->params.place_params.tile_id;
    unsigned int penguin_player_id = gw->w.players[player_id].nb_penguins_placed;

    if (gw->w.players[player_id].nb_penguins_placed >= gw->w.players[player_id].nb_penguins_max) {
      register_action_failure("The strategy wanted to place a penguin but they were already all on the board.");
      return 0;
    }

    if (tile_id >= gw->w.grid_board.nb_tiles) {
      register_action_failure("The strategy wanted to place a penguin on an invalid tile_id.");
      return 0;
    }

    if (gw->w.grid_board.tiles[tile_id].state != FREE) {
      register_action_failure("The strategy wanted to place a penguin on an already taken tile.");
      return 0;
    }

    assert(gw->w.players[player_id].penguins[penguin_player_id].tile_id == -1);
    assert(gw->current_round <= gw->w.nb_players * gw->w.nb_penguins_per_player);

    /* Alors cette action de placement est valide. */
  }

  /* Sinon si la stratégie a demandé un déplacement de pingouin : */
  else if (ga->kind == ACTION_MOVE) {
    unsigned int player_id = gw->current_player_id;
    unsigned int penguin_player_id = ga->params.move_params.penguin_player_id;
    unsigned int direction = ga->params.move_params.direction;
    unsigned int distance = ga->params.move_params.distance;

    if (gw->w.players[player_id].nb_penguins_placed < gw->w.players[player_id].nb_penguins_max) {
      register_action_failure("The strategy wanted to move a penguin but they were not all on the board.");
      return 0;
    }

    if (penguin_player_id >= gw->w.nb_penguins_per_player) {
      register_action_failure("The strategy wanted to move a penguin whose id does not exist.");
      return 0;
    }

    if (gw->w.players[player_id].penguins[penguin_player_id].tile_id == -1) {
      register_action_failure("The strategy wanted to move a penguin which was not placed.");
      return 0;
    }

    if (gw->w.players[player_id].penguins[penguin_player_id].is_blocked) {
      register_action_failure("The strategy wanted to move a penguin which was blocked.");
      return 0;
    }

    if (direction >= gw->w.grid_board.tessel_grid.nb_directions) {
      register_action_failure("The strategy wanted to move a penguin toward an invalid direction (too high).");
      return 0;
    }

    if (distance >= gw->w.grid_board.nb_tiles) {
      register_action_failure("The strategy wanted to move a penguin with an invalid distance (hard limit reached).");
      return 0;
    }

    if (0 > grid__validate_path(
          &gw->w.grid_board,
          gw->w.players[player_id].penguins[penguin_player_id].tile_id,
          direction,
          distance)) {
      register_action_failure("The strategy wanted to move a penguin to an invalid location : invalid path.");
      return 0;
    }

    /* Alors cette action de déplacement est valide. */
  }

  /* Sinon cette action n'est pas connue : c'est une erreur. */
  else {
    return 0;
  }

  return 1;
}


/* Applicateur d'action au modèle de jeu */
int game_world__apply_action (struct game_world *gw, struct game_action *ga) {
  /* warning: Do check the action validity beforehand !!! */
  log_debug("started", NULL);

  /* Raccourcis */
  struct player* pl = &gw->w.players[gw->current_player_id];
  struct grid *gd = &gw->w.grid_board;

  switch (ga->kind) {
    /* Si c'est une action de placement : */
    case ACTION_PLACE: {
      unsigned int tile_id = ga->params.place_params.tile_id;

      pl->penguins[pl->nb_penguins_placed].tile_id = tile_id;
      assert(pl->nb_penguins_placed == pl->penguins[pl->nb_penguins_placed].penguin_player_id);
      gd->tiles[tile_id].penguin_id = pl->nb_penguins_placed;
      gd->tiles[tile_id].state = TAKEN;
      pl->nb_penguins_placed++;

    	break;
		}

    /* Si c'est une action de déplacement : */
    case ACTION_MOVE: {
      unsigned int penguin_player_id = ga->params.move_params.penguin_player_id;
      unsigned int penguin_id = pl->penguins[penguin_player_id].id;
      unsigned int direction = ga->params.move_params.direction;
      unsigned int distance = ga->params.move_params.distance;
      unsigned int penguin_tile_id = pl->penguins[penguin_player_id].tile_id;

      struct tile* starting_tile = &gd->tiles[penguin_tile_id];
      unsigned int starting_tile_id = penguin_tile_id;

      struct tcoord from_tc, to_tc;
      MBvertex *v;
      struct tile* next_tile = starting_tile;
      unsigned int next_tile_id = starting_tile_id;

      for (int d = 0; d < distance; d++) {
        gd->tessel_grid.init_tcoord(&gd->tessel_grid, next_tile_id, &from_tc);

        gd->tessel_grid.set_tcoord_for_dir(&gd->tessel_grid, &to_tc, &from_tc, direction);

        v = MBgraph2_get_vertex(gd->graph, to_tc.s);
        if (v == NULL) {
          log_error("to_tc.s: %s", to_tc.s);
        }
        assert(v != NULL);
        next_tile = MBvertex_get_data(v);
        assert(next_tile != NULL);
        next_tile_id = next_tile->id;
      }

      /* Scores */
      player__score(pl, starting_tile->nb_fish, distance);
      pl->penguins[penguin_player_id].tile_id = next_tile_id;

      grid__takeoff_tile(gd, starting_tile_id);
      grid__landing_tile(gd, next_tile_id, penguin_id);

      break;
		}

    default:
			log_error("Action type not implemented!", NULL);
      exit(EXIT_FAILURE);
			break;
	}

  log_debug("finished", NULL);
  return 0;
}


/* Préparateur du tour suivant */
void game_world__prepare_next_round (struct game_world *gw) {
  gw->current_player_id = (gw->current_player_id + 1) % gw->w.nb_players;
  gw->current_round++;
}


/* Méthode de déclassement d'un joueur lors d'une erreur */
void game_world__nuke_current_player (struct game_world *gw) {
  log_debug("nuking player %u", gw->current_player_id);
  gw->w.players[gw->current_player_id].score_fishes = 0;
  /* One generously let the cheater keep the other secondary scores. */
}


/* Trieur des joueurs d'après leurs scores

  Le classement est établi en utilisant la fonction GNU qsort : elle a seulement
  besoin de la taille des données à traiter et d'une méthode de comparaison */
void game_world__rank_score_players (struct game_world *gw) {
  log_debug("", NULL);

  qsort(gw->rankings, gw->w.nb_players, sizeof(struct player *), player__cmp_rank);
}


/* Actualisateur de pingouin bloqué */
void game_world__penguin_check_blocked (struct game_world *gw, struct penguin *pg) { // not exported
  int tile_id = pg->tile_id;

  if (tile_id != -1) {
    assert(tile_id < gw->w.grid_board.nb_tiles);
    pg->is_blocked = (!grid__has_free_neighbours(&gw->w.grid_board, pg->tile_id));
  }
  else {
    pg->is_blocked = 0;
  }
}


/* Actualisateur pour mettre à jour le blocage de tous les pingouins */
void game_world__check_blocked_penguins (struct game_world *gw) {

  /* Pour chaque joueur/pingouin : */
  for (int pl_id = 0; pl_id < gw->w.nb_players; pl_id++) {

    for (int pg_id = 0; pg_id < gw->w.players[pl_id].nb_penguins_max; pg_id++) {

      /* Mise à jour de son état bloqué. */
      game_world__penguin_check_blocked(gw, &gw->w.players[pl_id].penguins[pg_id]);

      if (gw->w.players[pl_id].penguins[pg_id].tile_id != -1) {
        int free = grid__nb_free_neighbours(&gw->w.grid_board, gw->w.players[pl_id].penguins[pg_id].tile_id);

        log_trace("player/penguin=%d/%d : is_blocked=%d (free=%d) (first=%d)", pl_id,
          pg_id, gw->w.players[pl_id].penguins[pg_id].is_blocked, free,
          grid__get_first_free_neighbour(&gw->w.grid_board, gw->w.players[pl_id].penguins[pg_id].tile_id));
      }
      else {
        log_trace("player/penguin=%d/%d : is_blocked=%d (not placed)", pl_id, pg_id, gw->w.players[pl_id].penguins[pg_id].is_blocked);
      }
    }
  }
}


/* Vérificateur de fin de jeu

  Renvoie
    1 si le modèle ne doit pas continuer à évoluer,
    0 sinon
*/
int game_world__is_finished (struct game_world *gw) {
  for (int pl_id = 0; pl_id < gw->w.nb_players; pl_id++) {
    if (!player__is_finished(&gw->w.players[pl_id])) {
      return 0;
    }
  }

  return 1;
}


/* Vérificateur de fin de partie pour le joueur courant

  Renvoie
    1 si le joueur courant peut continuer à jouer,
    0 sinon
*/
int game_world__current_player_can_play (struct game_world *gw) {
  return (!player__is_finished(&gw->w.players[gw->current_player_id]));
}


/* Afficheur du classement final des scores

  Renvoie l'identifiant du joueur ayant le meilleur score
*/
int game_world__report_rankings (struct game_world *gw) {

  fprintf(stdout, "Final Rankings:\n");

  /* Pour chaque joueur : */
  for (int i = 0; i < gw->w.nb_players; i++) {

    /* Affichage de score du joueurs et de ses informations */
    fprintf(stdout, "  %2d. # player(%2u) [%3u |%3u |%3u ] : %s\n", i + 1,
      gw->rankings[i]->id + 1,
      gw->rankings[i]->score_fishes,
      gw->rankings[i]->score_tiles,
      gw->rankings[i]->score_distance,
      gw->rankings[i]->name);
  }
  fflush(stdout);

  return gw->rankings[0]->id;
}
