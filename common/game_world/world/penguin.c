
#include "penguin.h"


/* Initialisateur de pingouin */
void penguin__init (struct penguin *pg, unsigned int player_id, unsigned int penguin_player_id) {
  static int nb_penguins = 0;
  pg->id = nb_penguins++;
  assert(nb_penguins < (MAX_PLAYERS * MAX_PENGUINS_PER_PLAYER));
  pg->player_id = player_id;
  pg->penguin_player_id = penguin_player_id;

  pg->tile_id = -1; /* Non placé */

  pg->is_blocked = 0;
}


/* Méthode de copie de pingouins */
void penguin__copy (struct penguin *pg_dst, struct penguin *pg_src) {
  pg_dst->id = pg_src->id;
  pg_dst->player_id = pg_src->player_id;
  pg_dst->penguin_player_id = pg_src->penguin_player_id;
  pg_dst->tile_id = pg_src->tile_id;
  pg_dst->is_blocked = pg_src->is_blocked;
}


/* Déstructeur de pingouin */
void penguin__destroy (struct penguin *pg) {
}


/* Vérificateur de pingouin bloqué */
int penguin__is_finished (struct penguin *pg) {
  return pg->is_blocked;
}
