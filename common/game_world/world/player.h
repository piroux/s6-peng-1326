
#ifndef  __PLAYER_H__
#define  __PLAYER_H__

#include "penguin.h"
#include "world_conf.h"


struct player {
  unsigned int id;

  /* Nom du joueur (path de la stratégie) */
  char *name;

  /* Pingouins du joueur */
  struct penguin penguins[MAX_PENGUINS_PER_PLAYER];

  /* Nombres maximal et placés de pingouins */
  unsigned int nb_penguins_max;
  unsigned int nb_penguins_placed;

  /* Scores du joueur */
  unsigned int score_fishes;
  unsigned int score_tiles;
  unsigned int score_distance;
};


void player__init (struct player *pl, int nb_penguins_per_player, char *name);
void player__copy (struct player *pl_dst, struct player *pl_src);
void player__destroy (struct player *pl);

void player__score (struct player *pl, unsigned int fishes, unsigned int distance);
int player__cmp_rank (const void *pl1_, const void *pl2_);
int player__is_finished (struct player *pl);


#endif
