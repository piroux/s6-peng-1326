
#ifndef _UTILS_LOG_H_
#define _UTILS_LOG_H_

//#define _POSIX_C_SOURCE 200809L


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>
#include <unistd.h>
#include <stdarg.h>
#include <inttypes.h>
#include <math.h>


enum log_level {
  TRACE,
  DEBUG,
  INFO,
  WARNING,
  ERROR,
  CRITICAL,
  LOG_LEVELS
};

#define USE_LOG 1

#define LOG_LEVEL TRACE

int ok_log(enum log_level level);

#define log_raw_if(level, f) do { \
  if (ok_log(level)) f; \
} while (0)


void log_fatal(char* msg);
void log_msg(enum log_level level_id, const char *func, const char *fmt, ...);


#define log_trace(format, ...)    log_msg (TRACE,     __func__, format, ## __VA_ARGS__)
#define log_debug(format, ...)    log_msg (DEBUG,     __func__, format, ## __VA_ARGS__)
#define log_info(format, ...)     log_msg (INFO,      __func__, format, ## __VA_ARGS__)
#define log_warning(format, ...)  log_msg (WARNING,   __func__, format, ## __VA_ARGS__)
#define log_error(format, ...)    log_msg (ERROR,     __func__, format, ## __VA_ARGS__)
#define log_critical(format, ...) log_msg (CRITICAL,  __func__, format, ## __VA_ARGS__)


/* http://mainisusuallyafunction.blogspot.fr/2012/01/embedding-gdb-breakpoints-in-c-source.html */
#define EMBED_BREAKPOINT \
    asm("0:"                              \
        ".pushsection embed-breakpoints;" \
        ".quad 0b;"                       \
        ".popsection;")

#endif
